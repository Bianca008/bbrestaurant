﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBRestaurant.Models.EntityLayer
{
    class Logger : BasePropertyChanged
    {
        public enum User
        {
            NOT_CONNECTED,
            CLIENT,
            EMPLOYEE
        }

        public Logger(string mail, string password)
        {
            Logger.Mail = mail;
            Logger.Password = password;
        }

        public static string Mail
        {
            get;
            set;
        }

        public static string Password
        {
            get;
            set;
        }

        public static User UserType
        {
            get;
            set;
        }
    }
}
