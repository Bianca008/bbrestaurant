﻿using System;
using System.Collections.ObjectModel;

namespace BBRestaurant.Models.EntityLayer
{
    class Product : BasePropertyChanged
    {
        string m_productName;
        string m_quantityProduct;
        string m_totalQuantityProduct;
        string m_price;
        string m_description;
        Category m_category;

        ObservableCollection<string> m_allergens;
        ObservableCollection<string> m_images;

        public Product()
        {
            m_allergens = new ObservableCollection<string>();
            m_images = new ObservableCollection<string>();
            m_category = new Category();
        }

        public string Name
        {
            get => m_productName;
            set
            {
                m_productName = value;
                NotifyPropertyChanged("Name");
            }
        }

        public string Quantity
        {
            get => m_quantityProduct;
            set
            {
                m_quantityProduct = value;
                NotifyPropertyChanged("Quantity");
            }
        }

        public string TotalQuantity
        {
            get => m_totalQuantityProduct;
            set
            {
                m_totalQuantityProduct = value;
                NotifyPropertyChanged("TotalQuantity");
            }
        }

        public string Price
        {
            get => m_price;
            set
            {
                m_price = value;
                NotifyPropertyChanged("Price");
            }
        }

        public string Description
        {
            get => m_description;
            set
            {
                m_description = value;
                NotifyPropertyChanged("Description");
            }
        }

        public Category Category
        {
            get => m_category;
            set
            {
                m_category = value;
                NotifyPropertyChanged("Category");
            }
        }

        public ObservableCollection<string> Allergens
        {
            get => m_allergens;
            set
            {
                m_allergens = value;
                NotifyPropertyChanged("Allergens");
            }
        }

        public ObservableCollection<string> Images
        {
            get => m_images;
            set
            {
                m_images = value;
                NotifyPropertyChanged("Images");
            }
        }

        private bool CheckIsPositiveDouble(string value)
        {
            double result;

            if (Double.TryParse(value, out result) && result > 0)
                return true;

            return false;
        }

        private bool CheckIsPositiveInt(string value)
        {
            int result;

            if (int.TryParse(value, out result) && result > 0)
                return true;

            return false;
        }

        public bool NotEmptyAndIncorrectFields()
        {
            if (Name == "") return false;
            if (Quantity == "" || CheckIsPositiveInt(Quantity) == false) return false;
            if (TotalQuantity == "" || CheckIsPositiveInt(TotalQuantity) == false) return false;
            if (Description == "") return false;
            if (Price == "" || CheckIsPositiveDouble(Price) == false) return false;
            if (m_category == null) return false;
            if (Images.Count == 0) return false;

            return true;
        }
    }
}
