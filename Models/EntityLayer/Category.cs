﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBRestaurant.Models.EntityLayer
{
    class Category : BasePropertyChanged
    {
        private int m_idCategory;
        private string m_categoryName;

        public int CategoryId
        {
            get
            {
                return m_idCategory;
            }
            set
            {
                m_idCategory = value;
                NotifyPropertyChanged("CategoryId");
            }
        }

        public string CategoryName
        {
            get
            {
                return m_categoryName;
            }
            set
            {
                m_categoryName = value;
                NotifyPropertyChanged("CategoryName");
            }
        }
    }
}
