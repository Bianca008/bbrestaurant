﻿using System.Collections.ObjectModel;
using System.Configuration;

namespace BBRestaurant.Models.EntityLayer
{
    class PriceCalculator 
    {
        static public double ProductsPrice
        {
            get;
            set;
        }

        static public double DeliveryPrice
        {
            get;
            set;
        }

        static public double Discount
        {
            get;
            set;
        }

        static public double GenerateDiscountsAndDelivery(ObservableCollection<ClientProduct> Products,
            bool applyDiscountForTime)
        {
            float price = 0;

            for (int index = 0; index < Products.Count; ++index)
                price += Products[index].Price * Products[index].NumberOfPieces;

            ProductsPrice = price;

            if (price < float.Parse(ConfigurationManager.
                AppSettings["totalPriceSmallerThan"]))
                DeliveryPrice = float.Parse(ConfigurationManager.
                    AppSettings["deliveryPrice"]);

            if (price > float.Parse(ConfigurationManager.
                AppSettings["totalPriceBiggerThan"]) || applyDiscountForTime == true)
                Discount = price * float.Parse(ConfigurationManager.
                    AppSettings["discountPercentCommand"]) / 100;

            return ProductsPrice + DeliveryPrice - Discount;
        }
    }
}
