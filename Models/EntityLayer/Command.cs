﻿using System;
using System.Collections.ObjectModel;
using System.Configuration;

namespace BBRestaurant.Models.EntityLayer
{
    class Command : BasePropertyChanged
    {
        int m_idCommand;
        int m_idClient;
        string m_firstName;
        string m_lastName;
        string m_phone;
        string m_status;
        string m_code;
        DateTime m_date;
        int m_duration;
        string m_adress;

        ObservableCollection<ClientProduct> m_products;

        public Command()
        {
            m_products = new ObservableCollection<ClientProduct>();
        }

        public int CommandId
        {
            get
            {
                return m_idCommand;
            }
            set
            {
                m_idCommand = value;
                NotifyPropertyChanged("CommandId");
            }
        }

        public int ClientId
        {
            get
            {
                return m_idClient;
            }
            set
            {
                m_idClient = value;
                NotifyPropertyChanged("ClientId");
            }
        }

        public string FirstName
        {
            get
            {
                return m_firstName;
            }
            set
            {
                m_firstName = value;
                NotifyPropertyChanged("FirstName");
            }
        }

        public string LastName
        {
            get
            {
                return m_lastName;
            }
            set
            {
                m_lastName = value;
                NotifyPropertyChanged("LastName");
            }
        }

        public string Name
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }

        public string Phone
        {
            get
            {
                return m_phone;
            }
            set
            {
                m_phone = value;
                NotifyPropertyChanged("Phone");
            }
        }

        public string Status
        {
            get
            {
                return m_status;
            }
            set
            {
                m_status = value;
                NotifyPropertyChanged("Status");
            }
        }

        public string Code
        {
            get
            {
                return m_code;
            }
            set
            {
                m_code = value;
                NotifyPropertyChanged("Code");
            }
        }

        public DateTime CommandDate
        {
            get
            {
                return m_date;
            }
            set
            {
                m_date = value;
                NotifyPropertyChanged("CommandDate");
            }
        }

        public DateTime ArriveDate
        {
            get
            {
                DateTime arriveDate = m_date;
                arriveDate = arriveDate.AddMinutes(m_duration);
                return arriveDate;
            }
        }

        public int Duration
        {
            get
            {
                return m_duration;
            }
            set
            {
                m_duration = value;
                NotifyPropertyChanged("Duration");
            }
        }

        public string Adress
        {
            get
            {
                return m_adress;
            }
            set
            {
                m_adress = value;
                NotifyPropertyChanged("Adress");
            }
        }

        public float TotalPrice
        {
            get
            {
                return (float)Math.Round(PriceCalculator.GenerateDiscountsAndDelivery(Products, false), 2);
            }
        }

        public float ProductsPrice
        {
            get
            {
                return (float)Math.Round(PriceCalculator.ProductsPrice, 2);
            }
            set
            {
                PriceCalculator.ProductsPrice = value;
                NotifyPropertyChanged("ProductsPrice");
            }
        }

        public float DeliveryPrice
        {
            get
            {
                return (float)Math.Round(PriceCalculator.DeliveryPrice, 2);
            }
            set
            {
                PriceCalculator.DeliveryPrice = value;
                NotifyPropertyChanged("DeliveryPrice");
            }
        }

        public float Discount
        {
            get
            {
                return (float)Math.Round(PriceCalculator.Discount, 2);
            }
            set
            {
                PriceCalculator.Discount = value;
                NotifyPropertyChanged("Discount");
            }
        }

        public ObservableCollection<ClientProduct> Products
        {
            get
            {
                return m_products;
            }
            set
            {
                m_products = value;
                NotifyPropertyChanged("Products");
            }
        }
    }
}
