﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBRestaurant.Models.EntityLayer
{
    class ProductStock : BasePropertyChanged
    {
        string m_name;
        int m_totalQuantity;

        public string Name
        {
            get
            {
                return m_name;
            }
            set
            {
                m_name = value;
                NotifyPropertyChanged("Name");
            }
        }

        public int TotalQuantity
        {
            get
            {
                return m_totalQuantity;
            }
            set
            {
                m_totalQuantity = value;
                NotifyPropertyChanged("TotalQuantity");
            }
        }
    }
}
