﻿namespace BBRestaurant.Models.EntityLayer
{
    class ClientProduct :BasePropertyChanged
    {
        private string m_name;
        private int m_numberOfPieces;
        private float m_price;

        public string Name
        {
            get
            {
                return m_name;
            }
            set
            {
                m_name = value;
                NotifyPropertyChanged("Name");
            }
        }

        public int NumberOfPieces
        {
            get
            {
                return m_numberOfPieces;
            }
            set
            {
                m_numberOfPieces = value;
                NotifyPropertyChanged("NumberOfPieces");
            }
        }

        public float Price
        {
            get
            {
                return m_price;
            }
            set
            {
                m_price = value;
                NotifyPropertyChanged("Price");
            }
        }
    }
}
