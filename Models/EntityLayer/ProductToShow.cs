﻿using BBRestaurant.ViewModels;
using System;
using System.Collections.Generic;
using System.Windows.Input;

namespace BBRestaurant.Models.EntityLayer
{
    class ProductToShow : BasePropertyChanged
    {
        private int m_idProduct;
        private string m_name;
        private string m_description;
        private string m_disponibility;
        private float m_price;
        private int m_index;
        private List<string> m_quantity;
        private List<string> m_images;
        private List<string> m_alergens;

        public ProductToShow()
        {
            m_quantity = new List<string>();
            m_images = new List<string>();
            m_alergens = new List<string>();
            m_disponibility = "";
        }

        public int ProductId
        {
            get
            {
                return m_idProduct;
            }
            set
            {
                m_idProduct = value;
                NotifyPropertyChanged("ProductId");
            }
        }

        public string Name
        {
            get
            {
                return m_name;
            }
            set
            {
                m_name = value;
                NotifyPropertyChanged("Name");
            }
        }

        public string Description
        {
            get
            {
                return m_description;
            }
            set
            {
                m_description = value;
                NotifyPropertyChanged("Description");
            }
        }

        public float Price
        {
            get
            {
                return (float)Math.Round(m_price, 2);
            }
            set
            {
                m_price = value;
                NotifyPropertyChanged("Price");
            }
        }

        public List<string> Quantity
        {
            get
            {
                return m_quantity;
            }
            set
            {
                m_quantity = value;
                NotifyPropertyChanged("Quantity");
            }
        }

        public List<string> PhotosList
        {
            get
            {
                return m_images;
            }
            set
            {
                m_images = value;
                NotifyPropertyChanged("PhotosList");
            }
        }

        public string ActualImage
        {
            get
            {
                return m_images[m_index];
            }
            set
            {
                m_images[m_index] = value;
                NotifyPropertyChanged("ActualImage");
            }
        }

        public string NextImage
        {
            get
            {
                if (m_index + 1 < m_images.Count)
                    return m_images[++m_index];
                else
                    return m_images[m_images.Count - 1];
            }
        }

        public string PreviousImage
        {
            get
            {
                if (m_index - 1 > 0)
                    return m_images[--m_index];
                else
                    return m_images[0];
            }
        }

        public List<string> AlergensList
        {
            get
            {
                return m_alergens;
            }
            set
            {
                m_alergens = value;
                NotifyPropertyChanged("AlergensList");
            }
        }

        public string Alergens
        {
            get
            {
                string alergens = "";

                for (int index = 0; index < m_alergens.Count; ++index)
                {
                    string intermediar = m_alergens[index].Replace(" ", "");
                    alergens += intermediar + " ";
                }

                if (alergens != "")
                    return "Alergeni: " + alergens;

                return "";
            }
        }

        public string Disponibility
        {
            get
            {
                return m_disponibility;
            }
            set
            {
                m_disponibility = value;
                NotifyPropertyChanged("Disponibility");
            }
        }

        ICommand m_arrowLeft;
        ICommand m_arrowRight;

        private void ArrowLeftPressed(object parameter)
        {
            ActualImage = PreviousImage;
        }

        public ICommand LeftArrow
        {
            get
            {
                if (m_arrowLeft == null)
                    m_arrowLeft = new RelayCommand<object>(ArrowLeftPressed);
                return m_arrowLeft;
            }
        }

        private void ArrowRightPressed(object parameter)
        {
            ActualImage = NextImage;
        }

        public ICommand RightArrow
        {
            get
            {
                if (m_arrowRight == null)
                    m_arrowRight = new RelayCommand<object>(ArrowRightPressed);
                return m_arrowRight;
            }
        }
    }
}
