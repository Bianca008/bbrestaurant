﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBRestaurant.Models.EntityLayer
{
    class Menu : BasePropertyChanged
    {
        string m_productName;
        string m_description;
        Category m_category;

        ObservableCollection<ProductStock> m_quantityProduct;
        ObservableCollection<string> m_images;

        public Menu()
        {
            m_quantityProduct = new ObservableCollection<ProductStock>();
            m_images = new ObservableCollection<string>();
            m_category = new Category();
        }

        public string Name
        {
            get => m_productName;
            set
            {
                m_productName = value;
                NotifyPropertyChanged("Name");
            }
        }

        public ObservableCollection<ProductStock> QuantityAndName
        {
            get => m_quantityProduct;
            set
            {
                m_quantityProduct = value;
                NotifyPropertyChanged("QuantityAndName");
            }
        }

        public string Description
        {
            get => m_description;
            set
            {
                m_description = value;
                NotifyPropertyChanged("Description");
            }
        }

        public Category Category
        {
            get => m_category;
            set
            {
                m_category = value;
                NotifyPropertyChanged("Category");
            }
        }

        public ObservableCollection<string> Images
        {
            get => m_images;
            set
            {
                m_images = value;
                NotifyPropertyChanged("Images");
            }
        }

        private bool CheckIsPositiveDouble(string value)
        {
            double result;

            if (Double.TryParse(value, out result) && result > 0)
                return true;

            return false;
        }

        private bool CheckIsPositiveInt(string value)
        {
            int result;

            if (int.TryParse(value, out result) && result > 0)
                return true;

            return false;
        }

        public bool NotEmptyAndIncorrectFields()
        {
            if (Name == "") return false;
            if (Description == "") return false;
            if (m_category == null) return false;
            if (Images.Count == 0) return false;

            return true;
        }
    }
}
