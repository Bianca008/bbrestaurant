﻿namespace BBRestaurant.Models.EntityLayer
{
    class Allergens : BasePropertyChanged
    {
        private int m_idAllergen;
        private string m_name;

        public int AllergenId
        {
            get
            {
                return m_idAllergen;
            }
            set
            {
                m_idAllergen = value;
                NotifyPropertyChanged("AllergenId");
            }
        }

        public string AllergenName
        {
            get
            {
                return m_name;
            }
            set
            {
                m_name = value;
                NotifyPropertyChanged("AllergenName");
            }
        }
    }
}
