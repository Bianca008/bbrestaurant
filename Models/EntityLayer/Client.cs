﻿namespace BBRestaurant.Models.EntityLayer
{
    class Client : BasePropertyChanged
    {
        private int m_idClient;
        private string m_firstName;
        private string m_lastName;
        private string m_phone;
        private string m_mail;
        private string m_password;

        public int ClientId
        {
            get
            {
                return m_idClient;
            }
            set
            {
                m_idClient = value;
                NotifyPropertyChanged("ClientId");
            }
        }

        public string FirstName
        {
            get
            {
                return m_firstName;
            }
            set
            {
                m_firstName = value;
                NotifyPropertyChanged("FirstName");
            }
        }

        public string LastName
        {
            get
            {
                return m_lastName;
            }
            set
            {
                m_lastName = value;
                NotifyPropertyChanged("LastName");
            }
        }

        public string PhoneNumber
        {
            get
            {
                return m_phone;
            }
            set
            {
                m_phone = value;
                NotifyPropertyChanged("PhoneNumber");
            }
        }

        public string Mail
        {
            get
            {
                return m_mail;
            }
            set
            {
                m_mail = value;
                NotifyPropertyChanged("Mail");
            }
        }

        public string Password
        {
            get
            {
                return m_password;
            }
            set
            {
                m_password = value;
                NotifyPropertyChanged("Password");
            }
        }
    }
}
