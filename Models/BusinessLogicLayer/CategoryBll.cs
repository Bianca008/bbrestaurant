﻿using BBRestaurant.Models.DataAccesLayer;
using BBRestaurant.Models.EntityLayer;
using System.Collections.ObjectModel;
using System.Linq;

namespace BBRestaurant.Models.BusinessLogicLayer
{
    class CategoryBll
    {
        public ObservableCollection<Category> CategoryList { get; set; }

        CategoryDal m_categoryDal = new CategoryDal();

        internal ObservableCollection<Category> GetAllCategories()
        {
            return m_categoryDal.GetAllCategories();
        }

        internal void AddCategory(string category)
        {
            foreach (var cat in CategoryList)
            {
                string name = cat.CategoryName.ToUpper();
                if (name.IndexOf(category.ToUpper()) != -1)
                    return;
            }

            m_categoryDal.AddCategory(category);
        }

        internal void UpdateCategory(string newCategory, string oldCategory)
        {
            m_categoryDal.UpdateCategory(newCategory, oldCategory);
        }

        internal void DeleteCategory(string categoryName)
        {
            m_categoryDal.DeleteCategory(categoryName);
        }
    }
}
