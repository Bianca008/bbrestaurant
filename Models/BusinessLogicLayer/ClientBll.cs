﻿using BBRestaurant.Models.DataAccesLayer;
using BBRestaurant.Models.EntityLayer;
using System;
using System.Collections.ObjectModel;
using System.Windows;

namespace BBRestaurant.Models.BusinessLogicLayer
{
    class ClientBll
    {

        ClientDal m_clientDal = new ClientDal();

        internal void Connect(Logger logger)
        {
            if (m_clientDal.ConnectClient() == true)
            {
                Logger.UserType = Logger.User.CLIENT;
                MessageBox.Show("Conectarea a fost un succes!");
            }
            else
                if (m_clientDal.ConnectEmployee() == true)
            {
                Logger.UserType = Logger.User.EMPLOYEE;
                MessageBox.Show("Conectarea a fost un succes!");
            }
            else
                MessageBox.Show("Conectarea a esuat!");
        }

        internal void AddClient(Client client)
        {
            if (String.IsNullOrEmpty(client.FirstName))
            {
                MessageBox.Show("Prenumele nu este valid.");
                return;
            }
            if (String.IsNullOrEmpty(client.LastName))
            {
                MessageBox.Show("Numele de familie nu este valid.");
                return;
            }
            if (String.IsNullOrEmpty(client.PhoneNumber) || client.PhoneNumber.Length != 10)
            {
                MessageBox.Show("Numarul de telefon nu este valid.");
                return;
            }
            if (String.IsNullOrEmpty(client.Mail))
            {
                MessageBox.Show("Mail-ul nu este valid.");
                return;
            }
            if (CheckValidMail(client.Mail) == true)
            {
                MessageBox.Show("Mail-ul acesta a mai fost folosit. Incercati o alta adresa de mail!");
                return;
            }
            if (String.IsNullOrEmpty(client.Password) || client.Password.Length < 8)
            {
                MessageBox.Show("Parola nu este valida.");
                return;
            }

            m_clientDal.AddClient(client);
            MessageBox.Show("Inregistrarea a fost facuta cu succes!");
        }

        private bool CheckValidMail(string mail)
        {
            ObservableCollection<string> mails = m_clientDal.GetAllEmails();

            for (int index = 0; index < mails.Count; ++index)
                if (mails[index].IndexOf(mail) != -1)
                    return true;

            return false;
        }
    }
}
