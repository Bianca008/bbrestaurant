﻿using BBRestaurant.Models.DataAccesLayer;
using BBRestaurant.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;

namespace BBRestaurant.Models.BusinessLogicLayer
{
    class ClientProductBll
    {
        public ObservableCollection<ClientProduct> Products { get; set; }

        ClientProductDal m_clientProductDal;

        public ClientProductBll()
        {
            Products = new ObservableCollection<ClientProduct>();
            m_clientProductDal = new ClientProductDal();
        }

        public bool IsProduct(string product)
        {
            ProductToShowBll productsBll = new ProductToShowBll();

            ObservableCollection<ProductToShow> products = productsBll.GetAllProducts();

            for (int index = 0; index < products.Count; ++index)
                if (product == products[index].Name)
                    return true;

            return false;
        }

        private List<string> GetQuantityOfProd(ClientProduct product)
        {
            ProductToShowBll productsBll = new ProductToShowBll();

            ObservableCollection<ProductToShow> products = productsBll.GetAllProducts();

            for (int index = 0; index < products.Count; ++index)
                if (product.Name == products[index].Name)
                {
                    return products[index].Quantity;
                }

            return new List<string>();
        }

        private int GetQuantityOfProductMenu(string product)
        {
            ProductToShowBll productsBll = new ProductToShowBll();

            ObservableCollection<ProductToShow> menus = productsBll.GetAllMenus();

            for (int index = 0; index < menus.Count; ++index)
            {
                List<KeyValuePair<string, int>> nameAndQuantityProd = productsBll.
                    GetQuantitiesOfMenuProducts(menus[index].Name);
                for (int indexProd = 0; indexProd < nameAndQuantityProd.Count; ++indexProd)
                    if (nameAndQuantityProd[indexProd].Key == product)
                        return nameAndQuantityProd[indexProd].Value;
            }

            return -1;
        }

        private bool CheckIsOkToAddProduct(ClientProduct product)
        {
            ProductToShowBll productsBll = new ProductToShowBll();

            int totalQuantityOfProduct = productsBll.GetQuantityOfProduct(product.Name);
            int totalQuantityOfProductCommand = 0;

            for (int index = 0; index < Products.Count; ++index)
            {
                if (Products[index].Name == product.Name)
                    totalQuantityOfProductCommand += Products[index].NumberOfPieces *
                        int.Parse(GetQuantityOfProd(product)[0]);

                if (IsProduct(Products[index].Name) == false)
                {
                    totalQuantityOfProductCommand += GetQuantityOfProductMenu(product.Name) *
                        Products[index].NumberOfPieces;
                }
            }

            totalQuantityOfProductCommand += product.NumberOfPieces * int.Parse(GetQuantityOfProd(product)[0]);

            if (totalQuantityOfProductCommand > totalQuantityOfProduct)
                return false;

            return true;
        }

        private bool CheckIsOkToAdd(ClientProduct product)
        {
            if (IsProduct(product.Name) == true)
            {
                return CheckIsOkToAddProduct(product);
            }
            else
            {
                ProductToShowBll productsBll = new ProductToShowBll();

                List<KeyValuePair<string, int>> nameAndQuantityProd = productsBll.
                   GetQuantitiesOfMenuProducts(product.Name);

                for (int indexProd = 0; indexProd < nameAndQuantityProd.Count; ++indexProd)
                {
                    ClientProduct prod = new ClientProduct
                    {
                        Name = nameAndQuantityProd[indexProd].Key,
                        NumberOfPieces = product.NumberOfPieces
                    };

                    if (CheckIsOkToAddProduct(prod) == false)
                        return false;
                }

                return true;
            }
        }

        internal void AddItemToCommand(ClientProduct product)
        {
            if (product != null)
            {
                if (CheckIsOkToAdd(product) == true)
                {
                    for (int index = 0; index < Products.Count; ++index)
                        if (Products[index].Name == product.Name)
                        {
                            Products[index].Price += Products[index].Price / Products[index].NumberOfPieces
                                * product.NumberOfPieces;
                            Products[index].NumberOfPieces += product.NumberOfPieces;
                            return;
                        }

                    ProductToShowBll productsBll = new ProductToShowBll();

                    ObservableCollection<ProductToShow> products = productsBll.GetAllProductsAndMenus();

                    for (int index = 0; index < products.Count; ++index)
                        if (products[index].Name == product.Name && products[index].Disponibility != "INDISPONIBIL")
                        {
                            product.Price = products[index].Price * product.NumberOfPieces;
                            Products.Add(product);
                        }
                }
                else
                    MessageBox.Show("Ingredientele sunt insuficiente!");
            }
        }

        internal void AddCommand(string adress)
        {
            int duration = 90;
            string code = m_clientProductDal.GenerateCommandCode();
            DateTime myDateTime = DateTime.Now;
            string data = myDateTime.ToString("yyyy-MM-dd HH:mm:ss");
            int id_client = m_clientProductDal.GetIdClient(Logger.Mail);

            m_clientProductDal.AddCommand(id_client, code, data, duration, adress);
            int id_command = m_clientProductDal.GetCommandId(code);

            for (int index = 0; index < Products.Count; ++index)
            {
                int productId = GetProductId(Products[index]);
                int menuId = GetMenuId(Products[index]);

                if (productId != -1)
                {
                    m_clientProductDal.AddProduct(id_command, productId, Products[index].NumberOfPieces);
                }
                else
                if (menuId != -1)
                {
                    m_clientProductDal.AddMenu(id_command, menuId, Products[index].NumberOfPieces);
                }
                UpdateQuantitiesProducts(Products[index]);
            }

            Products.Clear();

            //MessageBox.Show("Comanda a fost efectuata cu succes!");
        }

        private int GetProductId(ClientProduct product)
        {
            ProductToShowBll productToShowBll = new ProductToShowBll();
            ObservableCollection<ProductToShow> products = productToShowBll.GetAllProducts();

            for (int index = 0; index < products.Count; ++index)
                if (product.Name == products[index].Name)
                    return products[index].ProductId;

            return -1;
        }

        private int GetMenuId(ClientProduct product)
        {
            ProductToShowBll productToShowBll = new ProductToShowBll();
            ObservableCollection<ProductToShow> products = productToShowBll.GetAllMenus();

            for (int index = 0; index < products.Count; ++index)
                if (product.Name == products[index].Name)
                    return products[index].ProductId;

            return -1;
        }

        private void UpdateQuantitiesProducts(ClientProduct product)
        {
            ProductToShowBll productsBll = new ProductToShowBll();

            ObservableCollection<ProductToShow> products = productsBll.GetAllProducts();
            ObservableCollection<ProductToShow> menus = productsBll.GetAllMenus();

            for (int index = 0; index < products.Count; ++index)
                if (product.Name == products[index].Name)
                {
                    int totalQuantity = productsBll.GetQuantityOfProduct(product.Name);
                    if (totalQuantity > product.NumberOfPieces * int.Parse(products[index].Quantity[0]))
                        productsBll.UpdateProductQuantity(product.Name,
                            product.NumberOfPieces * int.Parse(products[index].Quantity[0]));
                }

            for (int index = 0; index < menus.Count; ++index)
                if (product.Name == menus[index].Name)
                {
                    List<KeyValuePair<string, int>> nameAndQuantityProd = productsBll.
                        GetQuantitiesOfMenuProducts(product.Name);

                    for (int indexProd = 0; indexProd < nameAndQuantityProd.Count; ++indexProd)
                        productsBll.UpdateProductQuantity(nameAndQuantityProd[indexProd].Key,
                            nameAndQuantityProd[indexProd].Value * product.NumberOfPieces);
                }
        }

        internal int GetIdClient(string email)
        {
            return m_clientProductDal.GetIdClient(email);
        }
    }
}
