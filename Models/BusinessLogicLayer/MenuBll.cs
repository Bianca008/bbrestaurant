﻿using BBRestaurant.Models.DataAccesLayer;
using BBRestaurant.Models.EntityLayer;

namespace BBRestaurant.Models.BusinessLogicLayer
{
    class MenuBll
    {
        MenuDal m_menuDal = new MenuDal();

        internal void AddMenu(Menu menu)
        {
            m_menuDal.AddMenu(menu);
        }

        internal void DeleteMenu(int menuId)
        {
            m_menuDal.DeleteMenu(menuId);
        }

        internal void UpdateMenu(ProductToShow oldMenu, Menu newMenu)
        {
            m_menuDal.UpdateMenu(oldMenu, newMenu);
        }
    }
}
