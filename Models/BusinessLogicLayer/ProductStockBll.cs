﻿using BBRestaurant.Models.DataAccesLayer;
using BBRestaurant.Models.EntityLayer;
using System.Collections.ObjectModel;

namespace BBRestaurant.Models.BusinessLogicLayer
{
    class ProductStockBll
    {
        public ObservableCollection<ProductStock> ProductsStock { get; set; }

        ProductStockDal m_productsStockDal = new ProductStockDal();

        internal ObservableCollection<ProductStock> GetAllProductsStock()
        {
            return m_productsStockDal.GetAllProductsStock();
        }
    }
}
