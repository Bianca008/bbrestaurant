﻿using BBRestaurant.Models.DataAccesLayer;
using BBRestaurant.Models.EntityLayer;
using System.Collections.ObjectModel;

namespace BBRestaurant.Models.BusinessLogicLayer
{
    class AllergensBll
    {
        public ObservableCollection<Allergens> AllergensList { get; set; }

        AllergensDal m_allergensDal = new AllergensDal();

        internal ObservableCollection<Allergens> GetAllAlergens()
        {
            return m_allergensDal.GetAllAllergens();
        }
    }
}
