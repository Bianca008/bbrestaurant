﻿using BBRestaurant.Models.DataAccesLayer;
using BBRestaurant.Models.EntityLayer;

namespace BBRestaurant.Models
{
    class ProductBll
    {
        ProductDal m_productDal = new ProductDal();

        internal void AddProduct(Product product)
        {
            m_productDal.AddProduct(product);
        }

        internal void DeleteProduct(int idProduct)
        {
            m_productDal.DeleteProduct(idProduct);
        }

        internal void UpdateProduct(ProductToShow oldProduct, Product newProduct)
        {
            m_productDal.UpdateProduct(oldProduct, newProduct);
        }
    }
}
