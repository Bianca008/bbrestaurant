﻿using BBRestaurant.Models.DataAccesLayer;
using BBRestaurant.Models.EntityLayer;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace BBRestaurant.Models.BusinessLogicLayer
{
    class ProductToShowBll
    {
        public ObservableCollection<ProductToShow> ProductsList { get; set; }

        ProductToShowDal m_productDal = new ProductToShowDal();

        internal ObservableCollection<ProductToShow> GetAllProducts()
        {
            return m_productDal.GetAllProducts();
        }

        internal ObservableCollection<ProductToShow> GetAllProductsFromCategory(string category)
        {
            return m_productDal.GetAllProductsFromCategory(category);
        }

        internal ObservableCollection<ProductToShow> GetAllMenus()
        {
            return m_productDal.GetAllMenus();
        }

        internal ObservableCollection<ProductToShow> GetAllProductsWithoutAllergen(
            string category,
            string allergen)
        {
            return m_productDal.GetAllProductsWithoutAlergen(category, allergen);
        }

        internal ObservableCollection<ProductToShow> GetAllProductsWithoutAllergen(
            string allergen)
        {
            return m_productDal.GetAllProductsWithoutAlergen(allergen);
        }

        internal ObservableCollection<ProductToShow> GetAllMenusWithoutAllergen(string allergen)
        {
            return m_productDal.GetAllMenusWithoutAlergen(allergen);
        }

        internal ObservableCollection<ProductToShow> SearchProductsFromWord(
           string category,
           string word)
        {
            return m_productDal.SearchProductsFromWord(category, word);
        }

        internal ObservableCollection<ProductToShow> GetAllProductsAndMenus()
        {
            ObservableCollection<ProductToShow> all = GetAllProducts();

            ObservableCollection<ProductToShow> menus = GetAllMenus();

            for (int index = 0; index < menus.Count; ++index)
                all.Add(menus[index]);

            return all;
        }

        internal int GetQuantityOfProduct(string name)
        {
            return m_productDal.GetQuantityOfProduct(name);
        }

        internal List<KeyValuePair<string, int>> GetQuantitiesOfMenuProducts(string name)
        {
            return m_productDal.GetQuantitiesOfMenuProducts(name);
        }

        internal void UpdateProductQuantity(string name, int quantity)
        {
            m_productDal.UpdateProductQuantity(name, quantity);
        }

        internal string GetCategoryOfProduct(int productId)
        {
            return m_productDal.GetCategoryOfProduct(productId);
        }

        internal string GetCategoryOfMenu(int menuId)
        {
            return m_productDal.GetCategoryOfMenu(menuId);
        }
    }
}
