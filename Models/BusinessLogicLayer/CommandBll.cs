﻿using BBRestaurant.Models.DataAccesLayer;
using BBRestaurant.Models.EntityLayer;
using System;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Windows;

namespace BBRestaurant.Models.BusinessLogicLayer
{
    class CommandBll
    {
        public ObservableCollection<Command> ClientCommandList { get; set; }

        public ObservableCollection<Command> EmployeeCommandsList { get; set; }

        CommandDal m_clientCommandDal = new CommandDal();

        internal ObservableCollection<Command> GetAllClientCommands()
        {
            return m_clientCommandDal.GetAllCommandForClient("GetAllCommandsForClient");
        }

        internal ObservableCollection<Command> GetAllClientActiveCommands()
        {
            return m_clientCommandDal.GetAllCommandForClient("GetAllActiveCommands");
        }

        internal ObservableCollection<Command> GetAllEmployeeCommands()
        {
            return m_clientCommandDal.GetAllCommandsForEmployee("GetAllCommandsForEmployee");
        }

        internal ObservableCollection<Command> GetAllActiveCommandsForEmployee()
        {
            return m_clientCommandDal.GetAllCommandsForEmployee("GetAllActiveCommandsForEmployee");
        }

        internal void CancelCommand(int id)
        {
            m_clientCommandDal.CancelCommand(id);
        }

        internal void UpdateStatus(Status status)
        {
            if (status != null)
            {
                m_clientCommandDal.UpdateCommand(status.Code, status.Stat);
                MessageBox.Show("Statusul comenzii a fost modificat cu succes!");
            }
            else
                MessageBox.Show("Statusul comenzii nu a fost modificat cu succes!");
        }

        public bool HasZCommandInTdays(int idClient)
        {
            ObservableCollection<Command> allCommands = GetAllEmployeeCommands();

            int numberOfCommands = 0;
            DateTime time = DateTime.Now.AddDays(-int.Parse(ConfigurationManager.
                AppSettings["timeForCommandsInDays"]));

            for (int index = 0; index < allCommands.Count; ++index)
                if (allCommands[index].ClientId == idClient && allCommands[index].CommandDate >= time)
                    ++numberOfCommands;

            if (numberOfCommands >= int.Parse(ConfigurationManager.
                AppSettings["nrOfCommands"]))
                return true;

            return false;
        }
    }
}
