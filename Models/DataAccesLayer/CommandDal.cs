﻿using BBRestaurant.Models.EntityLayer;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace BBRestaurant.Models.DataAccesLayer
{
    class CommandDal
    {
        internal ObservableCollection<Command> GetAllCommandsForEmployee(string storedProcedureName)
        {
            ClientProductDal clientDal = new ClientProductDal();

            SqlConnection con = DalHelper.Connection;

            try
            {
                SqlCommand cmd = new SqlCommand(storedProcedureName, con);

                ObservableCollection<Command> result = new ObservableCollection<Command>();
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Command employeeCommand = new Command();

                    employeeCommand.CommandId = (int)(reader[0]);
                    employeeCommand.ClientId = (int)(reader[1]);
                    employeeCommand.Status = reader.GetString(2).Replace(" ", "");
                    employeeCommand.Code = reader.GetString(3).Replace(" ", "");
                    employeeCommand.CommandDate = reader.GetDateTime(4);
                    employeeCommand.Adress = reader.GetString(5).Replace(" ", "");
                    employeeCommand.LastName = reader.GetString(6).Replace(" ", "");
                    employeeCommand.FirstName = reader.GetString(7).Replace(" ", "");
                    employeeCommand.Phone = reader.GetString(8).Replace(" ", "");

                    result.Add(employeeCommand);
                }
                reader.Close();

                for (int index = 0; index < result.Count; ++index)
                {
                    ObservableCollection<ClientProduct> prods = GetAllProductsForClient(
                        result[index].CommandId);
                    ObservableCollection<ClientProduct> menus = GetAllMenusForClient(
                        result[index].CommandId);

                    for (int indexProds = 0; indexProds < prods.Count; ++indexProds)
                        result[index].Products.Add(prods[indexProds]);

                    for (int indexMenu = 0; indexMenu < menus.Count; ++indexMenu)
                        result[index].Products.Add(menus[indexMenu]);
                }
                return result;
            }
            finally
            {
                con.Close();
            }
        }

        internal ObservableCollection<Command> GetAllCommandForClient(string storedProcedureName)
        {
            ClientProductDal clientDal = new ClientProductDal();
            int idClient = clientDal.GetIdClient(Logger.Mail);
            if (idClient == 0)
                return new ObservableCollection<Command>();

            SqlConnection con = DalHelper.Connection;

            try
            {
                SqlCommand cmd = new SqlCommand(storedProcedureName, con);
                cmd.Parameters.AddWithValue("@id_client", idClient);

                ObservableCollection<Command> result = new ObservableCollection<Command>();
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Command clientCommand = new Command();

                    clientCommand.CommandId = (int)(reader[0]);
                    clientCommand.ClientId = (int)(reader[1]);
                    clientCommand.Status = reader.GetString(2);
                    clientCommand.Code = reader.GetString(3);
                    clientCommand.CommandDate = reader.GetDateTime(4);
                    clientCommand.Duration = (int)(reader[5]);
                    clientCommand.Adress = reader.GetString(6);

                    result.Add(clientCommand);
                }
                reader.Close();

                for (int index = 0; index < result.Count; ++index)
                {
                    ObservableCollection<ClientProduct> prods = GetAllProductsForClient(
                        result[index].CommandId);
                    ObservableCollection<ClientProduct> menus = GetAllMenusForClient(
                        result[index].CommandId);

                    for (int indexProds = 0; indexProds < prods.Count; ++indexProds)
                        result[index].Products.Add(prods[indexProds]);

                    for (int indexMenu = 0; indexMenu < menus.Count; ++indexMenu)
                        result[index].Products.Add(menus[indexMenu]);
                }

                return result;
            }
            finally
            {
                con.Close();
            }
        }

        internal ObservableCollection<ClientProduct> GetAllProductsForClient(int commandId)
        {
            SqlConnection con = DalHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("GetAllProductsForClient", con);
                cmd.Parameters.AddWithValue("@id_comanda", commandId);

                ObservableCollection<ClientProduct> result = new ObservableCollection<ClientProduct>();
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ClientProduct clientProduct = new ClientProduct();
                    clientProduct.Name = reader.GetString(0);
                    clientProduct.Price = float.Parse(reader[1].ToString());
                    clientProduct.NumberOfPieces = (int)(reader[2]);
                    clientProduct.Price /= clientProduct.NumberOfPieces;
                    result.Add(clientProduct);
                }
                reader.Close();
                return result;
            }
            finally
            {
                con.Close();
            }
        }

        internal ObservableCollection<ClientProduct> GetAllMenusForClient(int commandId)
        {
            SqlConnection con = DalHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("GetAllMenusForCommand", con);
                cmd.Parameters.AddWithValue("@id_comanda", commandId);

                List<int> idMenus = new List<int>();
                ObservableCollection<ClientProduct> result = new ObservableCollection<ClientProduct>();
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ClientProduct clientProduct = new ClientProduct();
                    idMenus.Add((int)reader[0]);
                    clientProduct.Name = reader.GetString(1);
                    clientProduct.NumberOfPieces = (int)(reader[2]);

                    result.Add(clientProduct);
                }
                reader.Close();
                GeneratePriceAndQuantityForMenus(result, idMenus);

                return result;
            }
            finally
            {
                con.Close();
            }
        }

        private void GeneratePriceAndQuantityForMenus(ObservableCollection<ClientProduct> products,
            List<int> idMenus)
        {
            SqlConnection con = DalHelper.Connection;
            try
            {
                con.Open();
                for (int index = 0; index < products.Count; ++index)
                {
                    SqlCommand command = new SqlCommand("GetQuantityAndPricePerMenu", con);
                    command.Parameters.AddWithValue("@id_meniu", idMenus[index]);
                    command.CommandType = CommandType.StoredProcedure;

                    SqlDataReader readerCom = command.ExecuteReader();

                    while (readerCom.Read())
                    {
                        float value = float.Parse(readerCom[1].ToString());
                        products[index].Price = value / 100;
                        products[index].Price -= products[index].Price *
                            (float.Parse(ConfigurationManager.AppSettings["discountPercent"]) / 100);
                    }
                    readerCom.Close();
                }
            }
            finally
            {
                con.Close();
            }
        }

        internal void CancelCommand(int commandId)
        {
            using (SqlConnection con = DalHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("CancelCommand", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter id = new SqlParameter("@id_comanda", commandId);
                cmd.Parameters.Add(id);
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

        internal void UpdateCommand(string code, string status)
        {
            using (SqlConnection con = DalHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("UpdateCommandStatus", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter commandCode = new SqlParameter("@cod", code);
                SqlParameter commandStatus = new SqlParameter("@status", status);
                cmd.Parameters.Add(commandCode);
                cmd.Parameters.Add(commandStatus);
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }
}
