﻿using BBRestaurant.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBRestaurant.Models.DataAccesLayer
{
    class CategoryDal
    {
        internal ObservableCollection<Category> GetAllCategories()
        {
            SqlConnection con = DalHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("GetAllCategories", con);
                ObservableCollection<Category> result = new ObservableCollection<Category>();
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Category cat = new Category();
                    cat.CategoryId = (int)(reader[0]);
                    cat.CategoryName = reader.GetString(1);
                    result.Add(cat);
                }
                reader.Close();
                return result;
            }
            finally
            {
                con.Close();
            }
        }

        internal void AddCategory(string category)
        {
            using (SqlConnection con = DalHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("AddCategory", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter cat = new SqlParameter("@tip", category);          
                cmd.Parameters.Add(cat);
               
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

        internal void UpdateCategory(string newCategory, string oldCategory)
        {
            using (SqlConnection con = DalHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("UpdateCategory", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter categoryNew = new SqlParameter("@noul_tip", newCategory);
                SqlParameter categoryOld = new SqlParameter("@vechiul_tip", oldCategory);
                cmd.Parameters.Add(categoryNew);
                cmd.Parameters.Add(categoryOld);

                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

        internal void DeleteCategory(string categoryName)
        {
            using (SqlConnection con = DalHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("DeleteCategory", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter category = new SqlParameter("@tip", categoryName);
                cmd.Parameters.Add(category);

                con.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }
}
