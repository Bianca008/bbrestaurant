﻿using BBRestaurant.Models.EntityLayer;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace BBRestaurant.Models.DataAccesLayer
{
    class ProductStockDal
    {
        internal ObservableCollection<ProductStock> GetAllProductsStock()
        {
            SqlConnection con = DalHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("GetProductsStoc", con);
                cmd.Parameters.AddWithValue("@cantitate", int.Parse(
                    ConfigurationManager.AppSettings["smallerQuantityThan"]));

                ObservableCollection<ProductStock> result = new ObservableCollection<ProductStock>();
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ProductStock prod = new ProductStock();
                    prod.Name = reader.GetString(0);
                    prod.TotalQuantity = (int)(reader[1]);
                    result.Add(prod);
                }
                reader.Close();
                return result;
            }
            finally
            {
                con.Close();
            }
        }
    }
}
