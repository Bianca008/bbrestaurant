﻿using System.Configuration;
using System.Data.SqlClient;

namespace BBRestaurant.Models.DataAccesLayer
{
    class DalHelper
    {
        private static readonly string connectionString = ConfigurationManager.ConnectionStrings["myConStr"].ConnectionString;

        internal static SqlConnection Connection
        {
            get
            {
                return new SqlConnection(connectionString);
            }
        }
    }
}
