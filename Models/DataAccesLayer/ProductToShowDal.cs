﻿using BBRestaurant.Models.EntityLayer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace BBRestaurant.Models.DataAccesLayer
{
    class ProductToShowDal
    {
        private void GenerateAlergensForProducts(ObservableCollection<ProductToShow> products)
        {
            SqlConnection con = DalHelper.Connection;
            try
            {
                con.Open();
                for (int index = 0; index < products.Count; ++index)
                {
                    SqlCommand command = new SqlCommand("GetAlergensOfProduct", con);
                    command.Parameters.AddWithValue("@id_preparat", products[index].ProductId);
                    command.CommandType = CommandType.StoredProcedure;

                    SqlDataReader readerCom = command.ExecuteReader();

                    while (readerCom.Read())
                    {
                        products[index].AlergensList.Add(readerCom.GetString(0));
                    }
                    readerCom.Close();
                }
            }
            finally
            {
                con.Close();
            }
        }

        private void GenerateImagesForProducts(ObservableCollection<ProductToShow> products)
        {
            SqlConnection con = DalHelper.Connection;
            try
            {
                con.Open();
                for (int index = 0; index < products.Count; ++index)
                {
                    SqlCommand command = new SqlCommand("GetPhotosOfProduct", con);
                    command.Parameters.AddWithValue("@id_preparat", products[index].ProductId);
                    command.CommandType = CommandType.StoredProcedure;

                    SqlDataReader readerCom = command.ExecuteReader();

                    while (readerCom.Read())
                    {
                        products[index].PhotosList.Add(readerCom.GetString(0));
                    }
                    readerCom.Close();
                }
            }
            finally
            {
                con.Close();
            }
        }

        internal ObservableCollection<ProductToShow> GetAllProducts()
        {
            SqlConnection con = DalHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("GetProducts", con);
                ObservableCollection<ProductToShow> result = new ObservableCollection<ProductToShow>();
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ProductToShow prod = new ProductToShow();
                    prod.ProductId = (int)(reader[0]);
                    prod.Name = reader.GetString(1);
                    prod.Price = (float)(reader[2]);
                    prod.Quantity.Add((reader[3]).ToString());
                    prod.Description = reader.GetString(4);
                    result.Add(prod);
                }
                reader.Close();

                GenerateImagesForProducts(result);

                GenerateAlergensForProducts(result);

                GenerateProductsDisponibility(result);

                return result;
            }
            finally
            {
                con.Close();
            }
        }

        internal ObservableCollection<ProductToShow> GetAllProductsFromCategory(string category)
        {
            SqlConnection con = DalHelper.Connection;
            try
            {
                ObservableCollection<ProductToShow> result = new ObservableCollection<ProductToShow>();

                if (category.IndexOf("TOATE") != -1)
                {
                    result = GetAllProducts();
                    ObservableCollection<ProductToShow> menusList = new ObservableCollection<ProductToShow>();
                    menusList = GetAllMenus();

                    for (int index = 0; index < menusList.Count; ++index)
                        result.Add(menusList[index]);

                    return result;
                }

                if (category.IndexOf("MENIU") != -1)
                {
                    result = GetAllMenus();
                    return result;
                }

                SqlCommand cmd = new SqlCommand("GetAllProductsFromCategory", con);
                cmd.Parameters.AddWithValue("@tip", category);

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ProductToShow prod = new ProductToShow();
                    prod.ProductId = (int)(reader[0]);
                    prod.Name = reader.GetString(1);
                    prod.Price = (float)(reader[2]);
                    prod.Quantity.Add((reader[3]).ToString());
                    prod.Description = reader.GetString(4);
                    result.Add(prod);
                }
                reader.Close();

                GenerateImagesForProducts(result);

                GenerateAlergensForProducts(result);

                GenerateProductsDisponibility(result);

                ObservableCollection<ProductToShow> menus = GetAllMenusFromCategory(category);

                for (int index = 0; index < menus.Count; ++index)
                    result.Add(menus[index]);

                return result;
            }
            finally
            {
                con.Close();
            }
        }

        internal ObservableCollection<ProductToShow> GetAllMenusFromCategory(string category)
        {
            SqlConnection con = DalHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("GetMenusFromCategory", con);
                cmd.Parameters.AddWithValue("@tip", category);

                ObservableCollection<ProductToShow> result = new ObservableCollection<ProductToShow>();
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ProductToShow prod = new ProductToShow();
                    prod.ProductId = (int)(reader[0]);
                    prod.Name = reader.GetString(1);
                    prod.Description = reader.GetString(2);
                    result.Add(prod);
                }
                reader.Close();

                GenerateImagesForMenus(result);

                GenerateAlergensForMenus(result);

                GeneratePriceAndQuantityForMenus(result);

                GenerateMenusDisponibility(result);

                return result;
            }
            finally
            {
                con.Close();
            }
        }

        internal ObservableCollection<ProductToShow> GetAllMenus()
        {
            SqlConnection con = DalHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("GetAllMenus", con);
                ObservableCollection<ProductToShow> result = new ObservableCollection<ProductToShow>();
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ProductToShow prod = new ProductToShow();
                    prod.ProductId = (int)(reader[0]);
                    prod.Name = reader.GetString(1);
                    prod.Description = reader.GetString(2);
                    result.Add(prod);
                }
                reader.Close();

                GenerateImagesForMenus(result);

                GenerateAlergensForMenus(result);

                GeneratePriceAndQuantityForMenus(result);

                GenerateMenusDisponibility(result);

                return result;
            }
            finally
            {
                con.Close();
            }
        }

        private void GenerateImagesForMenus(ObservableCollection<ProductToShow> products)
        {
            SqlConnection con = DalHelper.Connection;
            try
            {
                con.Open();
                for (int index = 0; index < products.Count; ++index)
                {
                    SqlCommand command = new SqlCommand("GetPhotosOfMenu", con);
                    command.Parameters.AddWithValue("@id_meniu", products[index].ProductId);
                    command.CommandType = CommandType.StoredProcedure;

                    SqlDataReader readerCom = command.ExecuteReader();

                    while (readerCom.Read())
                    {
                        products[index].PhotosList.Add(readerCom.GetString(0));
                    }
                    readerCom.Close();
                }
            }
            finally
            {
                con.Close();
            }
        }

        private void GenerateAlergensForMenus(ObservableCollection<ProductToShow> products)
        {
            SqlConnection con = DalHelper.Connection;
            try
            {
                con.Open();
                for (int index = 0; index < products.Count; ++index)
                {
                    SqlCommand command = new SqlCommand("GetAlergensForMenu", con);
                    command.Parameters.AddWithValue("@id_meniu", products[index].ProductId);
                    command.CommandType = CommandType.StoredProcedure;

                    SqlDataReader readerCom = command.ExecuteReader();

                    while (readerCom.Read())
                    {
                        products[index].AlergensList.Add(readerCom.GetString(0));
                    }
                    readerCom.Close();
                }
            }
            finally
            {
                con.Close();
            }
        }

        private void GeneratePriceAndQuantityForMenus(ObservableCollection<ProductToShow> products)
        {
            for (int index = 0; index < products.Count; ++index)
            {
                List<KeyValuePair<string, int>> nameQuantity = 
                    GetQuantitiesOfMenuProducts(products[index].Name);

                for (int indexProduct = 0; indexProduct < nameQuantity.Count; ++indexProduct)
                    products[index].Quantity.Add(nameQuantity[indexProduct].Value + "g "+ nameQuantity[indexProduct].Key);
            }
            SqlConnection con = DalHelper.Connection;
            try
            {
                con.Open();
                for (int index = 0; index < products.Count; ++index)
                {
                    SqlCommand command = new SqlCommand("GetQuantityAndPricePerMenu", con);
                    command.Parameters.AddWithValue("@id_meniu", products[index].ProductId);
                    command.CommandType = CommandType.StoredProcedure;

                    SqlDataReader readerCom = command.ExecuteReader();

                    while (readerCom.Read())
                    {
                       // products[index].Quantity.Add((readerCom[0]).ToString());
                        float value = float.Parse(readerCom[1].ToString());
                        products[index].Price = value / 100;
                        products[index].Price -= products[index].Price *
                            (float.Parse(ConfigurationManager.AppSettings["discountPercent"]) / 100);
                    }
                    readerCom.Close();
                }
            }
            finally
            {
                con.Close();
            }
        }

        private void GenerateMenusDisponibility(ObservableCollection<ProductToShow> products)
        {
            SqlConnection con = DalHelper.Connection;
            try
            {
                con.Open();
                for (int index = 0; index < products.Count; ++index)
                {
                    SqlCommand command = new SqlCommand("MenuDisponibility", con);
                    command.Parameters.AddWithValue("@id_meniu", products[index].ProductId);
                    command.CommandType = CommandType.StoredProcedure;

                    SqlDataReader readerCom = command.ExecuteReader();

                    if (readerCom.HasRows)
                    {
                        readerCom.Read();
                        products[index].Disponibility = "INDISPONIBIL";
                    }
                    else
                        products[index].Disponibility = "";

                    readerCom.Close();
                }
            }
            finally
            {
                con.Close();
            }
        }

        private void GenerateProductsDisponibility(ObservableCollection<ProductToShow> products)
        {
            SqlConnection con = DalHelper.Connection;
            try
            {
                con.Open();
                for (int index = 0; index < products.Count; ++index)
                {
                    SqlCommand command = new SqlCommand("GetDisponibilityOfProduct", con);
                    command.Parameters.AddWithValue("@id_preparat", products[index].ProductId);
                    command.CommandType = CommandType.StoredProcedure;

                    SqlDataReader readerCom = command.ExecuteReader();

                    if (readerCom.HasRows)
                    {
                        readerCom.Read();
                        products[index].Disponibility = "INDISPONIBIL";
                    }
                    else
                        products[index].Disponibility = "";

                    readerCom.Close();
                }
            }
            finally
            {
                con.Close();
            }
        }

        internal ObservableCollection<ProductToShow> GetAllProductsWithoutAlergen(
            string category,
            string allergen)
        {
            ObservableCollection<ProductToShow> productsWithoutAllergen = GetAllProductsFromCategory(category);
            ObservableCollection<ProductToShow> products = new ObservableCollection<ProductToShow>();

            for (int index = 0; index < productsWithoutAllergen.Count; ++index)
            {
                bool okToAdd = true;
                for (int indexAllergen = 0; 
                    indexAllergen < productsWithoutAllergen[index].AlergensList.Count;
                    ++indexAllergen)
                    if (productsWithoutAllergen[index].AlergensList[indexAllergen].IndexOf(allergen) != -1)
                        okToAdd = false;
                if (okToAdd == true)
                    products.Add(productsWithoutAllergen[index]);
            }

            return products;
        }
        internal ObservableCollection<ProductToShow> GetAllProductsWithoutAlergen(
            string allergen)
        {
            SqlConnection con = DalHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("GetAllProductsWithoutAllergen", con);
                cmd.Parameters.AddWithValue("@denumire", allergen);

                ObservableCollection<ProductToShow> result = new ObservableCollection<ProductToShow>();

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ProductToShow prod = new ProductToShow();
                    prod.ProductId = (int)(reader[0]);
                    prod.Name = reader.GetString(1);
                    prod.Price = (float)(reader[2]);
                    prod.Quantity.Add((reader[3]).ToString());
                    prod.Description = reader.GetString(4);
                    result.Add(prod);
                }
                reader.Close();

                GenerateImagesForProducts(result);

                GenerateAlergensForProducts(result);

                GenerateProductsDisponibility(result);

                return result;
            }
            finally
            {
                con.Close();
            }
        }

        internal ObservableCollection<ProductToShow> GetAllMenusWithoutAlergen(
             string allergen)
        {
            SqlConnection con = DalHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("GetAllMenusWithoutAllergen", con);
                cmd.Parameters.AddWithValue("@denumire", allergen);
                ObservableCollection<ProductToShow> result = new ObservableCollection<ProductToShow>();
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ProductToShow prod = new ProductToShow();
                    prod.ProductId = (int)(reader[0]);
                    prod.Name = reader.GetString(1);
                    prod.Description = reader.GetString(2);
                    result.Add(prod);
                }
                reader.Close();

                GenerateImagesForMenus(result);

                GenerateAlergensForMenus(result);

                GeneratePriceAndQuantityForMenus(result);

                GenerateMenusDisponibility(result);

                return result;
            }
            finally
            {
                con.Close();
            }
        }

        internal ObservableCollection<ProductToShow> SearchProductsFromWord(
            string category,
            string word)
        {
            ObservableCollection<ProductToShow> productsList = GetAllProductsFromCategory(category);
            ObservableCollection<ProductToShow> products = new ObservableCollection<ProductToShow>();

            for (int index = 0; index < productsList.Count; ++index)
                if (productsList[index].Name.ToUpper().IndexOf(word.ToUpper()) != -1)
                    products.Add(productsList[index]);

            return products;
        }

        internal int GetQuantityOfProduct(string name)
        {
            SqlConnection con = DalHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("GetTotalQuantityOfProduct", con);
                cmd.Parameters.AddWithValue("@denumire", name);

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    int id = reader.GetInt32(0);
                    return id;
                }
                reader.Close();
            }
            finally
            {
                con.Close();
            }
            return 0;
        }

        internal string GetCategoryOfProduct(int productId)
        {
            SqlConnection con = DalHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("GetProductCategory", con);
                cmd.Parameters.AddWithValue("@id_produs", productId);

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    string category = reader.GetString(0);
                    return category;
                }
                reader.Close();
            }
            finally
            {
                con.Close();
            }
            return "";
        }

        internal string GetCategoryOfMenu(int menuId)
        {
            SqlConnection con = DalHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("GetMenuCategory", con);
                cmd.Parameters.AddWithValue("@id_meniu", menuId);

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    string category = reader.GetString(0);
                    return category;
                }
                reader.Close();
            }
            finally
            {
                con.Close();
            }
            return "";
        }

        internal List<KeyValuePair<string, int>> GetQuantitiesOfMenuProducts(string name)
        {
            SqlConnection con = DalHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("GetQuantitiesOfMenuProducts", con);
                cmd.Parameters.AddWithValue("@denumire", name);

                List<KeyValuePair<string, int>> result = new List<KeyValuePair<string, int>>();

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    string productName = reader.GetString(0);
                    int quantity = (int)(reader[1]);

                    result.Add(new KeyValuePair<string, int>(productName, quantity));
                }
                reader.Close();

                return result;
            }
            finally
            {
                con.Close();
            }
        }

        internal void UpdateProductQuantity(string name, int quantity)
        {
            using (SqlConnection con = DalHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("UpdateProductQuantity", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter productName = new SqlParameter("@denumire", name);
                SqlParameter quant = new SqlParameter("@cantitate", quantity);
                cmd.Parameters.Add(productName);
                cmd.Parameters.Add(quant);
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

    }
}
