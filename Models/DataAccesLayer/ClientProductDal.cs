﻿using BBRestaurant.Models.EntityLayer;
using System.Data;
using System.Data.SqlClient;

namespace BBRestaurant.Models.DataAccesLayer
{
    class ClientProductDal
    {
        internal string GenerateCommandCode()
        {
            SqlConnection con = DalHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("GenerateRandomCode", con);

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    string code = reader.GetString(0);
                    return code;
                }
                reader.Close();
            }
            finally
            {
                con.Close();
            }
            return "";
        }

        internal int GetCommandId(string code)
        {
            SqlConnection con = DalHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("GetCommandId", con);
                cmd.Parameters.AddWithValue("@cod", code);

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    int id = reader.GetInt32(0);
                    return id;
                }
                reader.Close();
            }
            finally
            {
                con.Close();
            }
            return 0;
        }

        internal int GetIdClient(string email)
        {
            SqlConnection con = DalHelper.Connection;
            if(Logger.Mail!=null)
            try
            {
                SqlCommand cmd = new SqlCommand("GetIdClient", con);
                cmd.Parameters.AddWithValue("@email", email);

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    int id = reader.GetInt32(0);
                    return id;
                }
                reader.Close();
            }
            finally
            {
                con.Close();
            }
            return 0;
        }

        internal void AddCommand(int id_client, string code, string date, int duration, string adress)
        {
            SqlConnection con = DalHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("AddCommand", con);
                cmd.Parameters.AddWithValue("@id_client", id_client);
                cmd.Parameters.AddWithValue("@cod", code);
                cmd.Parameters.AddWithValue("@data", date);
                cmd.Parameters.AddWithValue("@durata_livrare", duration);
                cmd.Parameters.AddWithValue("@adresa", adress);

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
            }
        }

        internal void AddProduct(int id_command, int id_product, int numberOfPieces)
        {
            SqlConnection con = DalHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("AddProductToCommand", con);
                cmd.Parameters.AddWithValue("@id_comanda", id_command);
                cmd.Parameters.AddWithValue("@id_preparat", id_product);
                cmd.Parameters.AddWithValue("@cantitate", numberOfPieces);

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
            }
        }

        internal void AddMenu(int id_command, int id_product, int numberOfPieces)
        {
            SqlConnection con = DalHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("AddMenuToCommand", con);
                cmd.Parameters.AddWithValue("@id_comanda", id_command);
                cmd.Parameters.AddWithValue("@id_preparat", id_product);
                cmd.Parameters.AddWithValue("@cantitate", numberOfPieces);

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                con.Close();
            }
        }
    }
}
