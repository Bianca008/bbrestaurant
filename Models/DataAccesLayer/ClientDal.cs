﻿using BBRestaurant.Models.EntityLayer;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;

namespace BBRestaurant.Models.DataAccesLayer
{
    class ClientDal
    {
        internal bool ConnectClient()
        {
            SqlConnection con = DalHelper.Connection;
            try
            {
                SqlCommand command = new SqlCommand("ConnectClient", con);
                command.Parameters.AddWithValue("@email", Logger.Mail);
                command.Parameters.AddWithValue("@parola", Logger.Password);

                command.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader readerCom = command.ExecuteReader();

                if (readerCom.HasRows)
                {
                    readerCom.Read();
                    readerCom.Close();
                    return true;
                }
                else
                {
                    readerCom.Close();
                    return false;
                }
            }
            finally
            {
                con.Close();
            }
        }

        internal bool ConnectEmployee()
        {
            SqlConnection con = DalHelper.Connection;
            try
            {
                SqlCommand command = new SqlCommand("ConnectEmployee", con);
                command.Parameters.AddWithValue("@email", Logger.Mail);
                command.Parameters.AddWithValue("@parola", Logger.Password);

                command.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader readerCom = command.ExecuteReader();

                if (readerCom.HasRows)
                {
                    readerCom.Read();
                    readerCom.Close();
                    return true;
                }
                else
                {
                    readerCom.Close();
                    return false;
                }
            }
            finally
            {
                con.Close();
            }
        }

        internal void AddClient(Client client)
        {
            using (SqlConnection con = DalHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("AddClient", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter firstName = new SqlParameter("@prenume", client.FirstName);
                SqlParameter lastName = new SqlParameter("@nume", client.LastName);
                SqlParameter phone = new SqlParameter("@telefon", client.PhoneNumber);
                SqlParameter mail = new SqlParameter("@email", client.Mail);
                SqlParameter password = new SqlParameter("@parola", client.Password);
                cmd.Parameters.Add(firstName);
                cmd.Parameters.Add(lastName);
                cmd.Parameters.Add(phone);
                cmd.Parameters.Add(mail);
                cmd.Parameters.Add(password);
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

        internal ObservableCollection<string> GetAllEmails()
        {
            SqlConnection con = DalHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("GetAllEmails", con);
                ObservableCollection<string> result = new ObservableCollection<string>();
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    string mail = "";
                    mail = reader.GetString(0);
                    result.Add(mail);
                }
                reader.Close();
                return result;
            }
            finally
            {
                con.Close();
            }
        }
    }
}
