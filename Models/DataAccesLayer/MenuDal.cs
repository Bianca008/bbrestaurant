﻿using BBRestaurant.Models.EntityLayer;
using System.Data;
using System.Data.SqlClient;

namespace BBRestaurant.Models.DataAccesLayer
{
    class MenuDal
    {
        internal void AddMenu(Menu menu)
        {
            using (SqlConnection con = DalHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("AddMenu", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter prodName = new SqlParameter("@denumire", menu.Name);
                SqlParameter prodCategoryId = new SqlParameter("@id_categorie", menu.Category.CategoryId);
                SqlParameter prodDescription = new SqlParameter("@descriere", menu.Description);
                cmd.Parameters.Add(prodName);
                cmd.Parameters.Add(prodCategoryId);
                cmd.Parameters.Add(prodDescription);

                con.Open();
                cmd.ExecuteNonQuery();
            }

            foreach (var image in menu.Images)
                AddPhotoForMenu(menu.Name, image);

            foreach (var prod in menu.QuantityAndName)
                AddProductForMenu(menu.Name, prod.Name, prod.TotalQuantity);
        }

        private void AddPhotoForMenu(string productName, string source)
        {
            using (SqlConnection con = DalHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("AddPhotoForMenuProd", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter prodName = new SqlParameter("@denumire", productName);
                SqlParameter prodImg = new SqlParameter("@poza", source);
                cmd.Parameters.Add(prodName);
                cmd.Parameters.Add(prodImg);

                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

        private void AddProductForMenu(string menuName, string productName, int cantitate)
        {
            using (SqlConnection con = DalHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("AddProdForMenu", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter nameProduct = new SqlParameter("@denumire_preparat", productName);
                SqlParameter nameMenu = new SqlParameter("@denumire_meniu", menuName);
                SqlParameter quantity = new SqlParameter("@cantitate", cantitate);
                cmd.Parameters.Add(nameProduct);
                cmd.Parameters.Add(nameMenu);
                cmd.Parameters.Add(quantity);

                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

        internal void DeleteMenu(int idMenu)
        {
            using (SqlConnection con = DalHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("DeleteMenu", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter id = new SqlParameter("@id_meniu", idMenu);
                cmd.Parameters.Add(id);

                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

        private void DeleteImagesForMenu(int menuId)
        {
            using (SqlConnection con = DalHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("DeleteImagesForMenu", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter id = new SqlParameter("@id_meniu", menuId);
                cmd.Parameters.Add(id);

                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

        private void DeleteProductsForMenu(int productId)
        {
            using (SqlConnection con = DalHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("DeleteProductsFromMenu", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter id = new SqlParameter("@id_meniu", productId);
                cmd.Parameters.Add(id);

                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

        internal void UpdateMenu(ProductToShow oldProduct, Menu newMenu)
        {
            using (SqlConnection con = DalHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("UpdateMenu", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter prodId = new SqlParameter("@id_meniu", oldProduct.ProductId);
                SqlParameter prodName = new SqlParameter("@denumire", newMenu.Name);
                SqlParameter prodCategoryId = new SqlParameter("@id_categorie", newMenu.Category.CategoryId);
                SqlParameter prodDescription = new SqlParameter("@descriere", newMenu.Description);
                cmd.Parameters.Add(prodId);
                cmd.Parameters.Add(prodName);
                cmd.Parameters.Add(prodCategoryId);
                cmd.Parameters.Add(prodDescription);

                con.Open();
                cmd.ExecuteNonQuery();
            }

            DeleteProductsForMenu(oldProduct.ProductId);
            DeleteImagesForMenu(oldProduct.ProductId);

            foreach (var image in newMenu.Images)
                AddPhotoForMenu(newMenu.Name, image);

            foreach (var quantity in newMenu.QuantityAndName)
                AddProductForMenu(newMenu.Name, quantity.Name, quantity.TotalQuantity);
        }
    }
}
