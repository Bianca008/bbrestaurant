﻿using BBRestaurant.Models.EntityLayer;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;

namespace BBRestaurant.Models.DataAccesLayer
{
    class AllergensDal
    {
        internal ObservableCollection<Allergens> GetAllAllergens()
        {
            SqlConnection con = DalHelper.Connection;
            try
            {
                SqlCommand cmd = new SqlCommand("GetAllAlergens", con);
                ObservableCollection<Allergens> result = new ObservableCollection<Allergens>();
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Allergens cat = new Allergens();
                    cat.AllergenId = (int)(reader[0]);
                    cat.AllergenName = reader.GetString(1);
                    result.Add(cat);
                }
                reader.Close();
                return result;
            }
            finally
            {
                con.Close();
            }
        }
    }
}
