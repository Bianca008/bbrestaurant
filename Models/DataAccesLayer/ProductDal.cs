﻿using BBRestaurant.Models.EntityLayer;
using System.Data;
using System.Data.SqlClient;

namespace BBRestaurant.Models.DataAccesLayer
{
    class ProductDal
    {
        internal void AddProduct(Product product)
        {
            using (SqlConnection con = DalHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("AddProduct", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter prodName = new SqlParameter("@denumire", product.Name);
                SqlParameter prodPrice = new SqlParameter("@pret", float.Parse(product.Price));
                SqlParameter prodQuantity = new SqlParameter("@cantitate", int.Parse(product.Quantity));
                SqlParameter prodTotalQuantity = new SqlParameter("@cantitate_totala", int.Parse(product.TotalQuantity));
                SqlParameter prodCategoryId = new SqlParameter("@id_categorie", product.Category.CategoryId);
                SqlParameter prodDescription = new SqlParameter("@descriere", product.Description);
                cmd.Parameters.Add(prodName);
                cmd.Parameters.Add(prodPrice);
                cmd.Parameters.Add(prodQuantity);
                cmd.Parameters.Add(prodTotalQuantity);
                cmd.Parameters.Add(prodCategoryId);
                cmd.Parameters.Add(prodDescription);

                con.Open();
                cmd.ExecuteNonQuery();
            }

            foreach (var image in product.Images)
                AddPhotoForProduct(product.Name, image);

            foreach (var allergen in product.Allergens)
                AddAllergenForProduct(product.Name, allergen);
        }

        private void AddPhotoForProduct(string productName, string source)
        {
            using (SqlConnection con = DalHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("AddPhotoToProduct", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter prodName = new SqlParameter("@denumire", productName);
                SqlParameter prodImg = new SqlParameter("@poza", source);
                cmd.Parameters.Add(prodName);
                cmd.Parameters.Add(prodImg);

                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

        private void AddAllergenForProduct(string productName, string allergenName)
        {
            using (SqlConnection con = DalHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("AddAllergenForProduct", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter prodName = new SqlParameter("@denumire_produs", productName);
                SqlParameter prodAllergen = new SqlParameter("@denumire_alergen", allergenName);
                cmd.Parameters.Add(prodName);
                cmd.Parameters.Add(prodAllergen);

                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

        internal void DeleteProduct(int idProduct)
        {
            using (SqlConnection con = DalHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("DeleteProduct", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter id = new SqlParameter("@id_preparat", idProduct);
                cmd.Parameters.Add(id);

                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

        private void DeleteImagesForProduct(int productId)
        {
            using (SqlConnection con = DalHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("DeleteImageForProduct", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter id = new SqlParameter("@id_preparat", productId);
                cmd.Parameters.Add(id);

                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

        private void DeleteAllergenForProduct(int productId)
        {
            using (SqlConnection con = DalHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("DeleteAllergenForProduct", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter id = new SqlParameter("@id_preparat", productId);
                cmd.Parameters.Add(id);

                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

        internal void UpdateProduct(ProductToShow oldProduct, Product newProduct)
        {
            using (SqlConnection con = DalHelper.Connection)
            {
                SqlCommand cmd = new SqlCommand("UpdateProduct", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter prodId = new SqlParameter("@id_preparat", oldProduct.ProductId);
                SqlParameter prodName = new SqlParameter("@denumire", newProduct.Name);
                SqlParameter prodPrice = new SqlParameter("@pret", float.Parse(newProduct.Price));
                SqlParameter prodQuantity = new SqlParameter("@cantitate", int.Parse(newProduct.Quantity));
                SqlParameter prodTotalQuantity = new SqlParameter("@cantitate_totala", int.Parse(newProduct.TotalQuantity));
                SqlParameter prodCategoryId = new SqlParameter("@id_categorie", newProduct.Category.CategoryId);
                SqlParameter prodDescription = new SqlParameter("@descriere", newProduct.Description);
                cmd.Parameters.Add(prodId);
                cmd.Parameters.Add(prodName);
                cmd.Parameters.Add(prodPrice);
                cmd.Parameters.Add(prodQuantity);
                cmd.Parameters.Add(prodTotalQuantity);
                cmd.Parameters.Add(prodCategoryId);
                cmd.Parameters.Add(prodDescription);

                con.Open();
                cmd.ExecuteNonQuery();
            }

            DeleteAllergenForProduct(oldProduct.ProductId);
            DeleteImagesForProduct(oldProduct.ProductId);

            foreach (var image in newProduct.Images)
                AddPhotoForProduct(newProduct.Name, image);

            foreach (var allergen in newProduct.Allergens)
                AddAllergenForProduct(newProduct.Name, allergen);
        }
    }
}
