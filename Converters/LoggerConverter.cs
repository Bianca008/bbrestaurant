﻿using BBRestaurant.Models.EntityLayer;
using System;
using System.Windows.Data;

namespace BBRestaurant.Converters
{
    class LoggerConverter : IMultiValueConverter
    {
        public object Convert(object[] values,
            Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (values[0] != null && values[1] != null)
            {
                Logger.Mail = values[0].ToString();
                Logger.Password = values[1].ToString();

                return new Logger(Logger.Mail, Logger.Password);
            }
            else
            {
                return null;
            }
        }
        public object[] ConvertBack(object value,
            Type[] targetTypes,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
