﻿using BBRestaurant.Models.EntityLayer;
using System;
using System.Windows.Controls;
using System.Windows.Data;

namespace BBRestaurant.Converters
{
    class ClientProductConevrter : IMultiValueConverter
    {
        public object Convert(object[] values,
            Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            ProductToShow prod = values[0] as ProductToShow;
            ComboBoxItem comboBox = values[1] as ComboBoxItem;
            if (prod != null && comboBox != null)
            {
                return new ClientProduct()
                {
                    Name = prod.Name,
                    NumberOfPieces = int.Parse(comboBox.Content.ToString())
                };
            }
            else
            {
                return null;
            }
        }

        public object[] ConvertBack(object value,
           Type[] targetTypes,
           object parameter,
           System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
