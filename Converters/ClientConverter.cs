﻿using BBRestaurant.Models.EntityLayer;
using System;
using System.Windows.Data;

namespace BBRestaurant.Converters
{
    class ClientConverter : IMultiValueConverter
    {
        public object Convert(object[] values,
            Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (values[0] != null && values[1] != null && values[2] != null && values[3] != null && values[4] != null)
            {
                return new Client()
                {
                    LastName = values[0].ToString(),
                    FirstName = values[1].ToString(),
                    PhoneNumber = values[2].ToString(),
                    Mail = values[3].ToString(),
                    Password = values[4].ToString()
                };
            }
            else
            {
                return null;
            }
        }
        public object[] ConvertBack(object value,
            Type[] targetTypes,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
