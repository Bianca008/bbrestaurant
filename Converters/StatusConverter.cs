﻿using BBRestaurant.Models.EntityLayer;
using System;
using System.Windows.Controls;
using System.Windows.Data;

namespace BBRestaurant.Converters
{
    class StatusConverter : IMultiValueConverter
    {
        public object Convert(object[] values,
           Type targetType,
           object parameter,
           System.Globalization.CultureInfo culture)
        {
            Command listBox = values[0] as Command;
            ComboBoxItem comboBox = values[1] as ComboBoxItem;

            if (listBox != null && comboBox != null)
            {
                string code = listBox.Code;
                string status = comboBox.Content.ToString();

                return new Status
                {
                    Code = code,
                    Stat = status
                };
            }
            else
            {
                return null;
            }
        }
        public object[] ConvertBack(object value,
            Type[] targetTypes,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
