﻿using BBRestaurant.Models.EntityLayer;
using System;
using System.Windows;

namespace BBRestaurant.View
{
    /// <summary>
    /// Interaction logic for LogInPage.xaml
    /// </summary>
    public partial class LogInPage : Window
    {
        private void CloseIfLogged(object sender, EventArgs e)
        {
            if (Logger.UserType == Logger.User.NOT_CONNECTED)
                this.Close();
        }

        public LogInPage()
        {
            InitializeComponent();
        }
    }
}
