﻿using BBRestaurant.Models.EntityLayer;
using System;
using System.Windows;

namespace BBRestaurant.View
{
    public partial class HomePage : Window
    {
        private void SetAllToHidden()
        {
            numberOfProducts.Visibility = Visibility.Hidden;
            addButton.Visibility = Visibility.Hidden;
            adress.Visibility = Visibility.Hidden;
            commandProducts.Visibility = Visibility.Hidden;
            finishCommand.Visibility = Visibility.Hidden;
            commandProductsClient.Visibility = Visibility.Hidden;
            cancelCommand.Visibility = Visibility.Hidden;
            employeeCommands.Visibility = Visibility.Hidden;
            statusCommand.Visibility = Visibility.Hidden;
            modifyStatus.Visibility = Visibility.Hidden;
            productsStock.Visibility = Visibility.Hidden;
            addCategory.Visibility = Visibility.Hidden;
            categoryText.Visibility = Visibility.Hidden;
            modifyCategory.Visibility = Visibility.Hidden;
            prodName.Visibility = Visibility.Hidden;
            category.Visibility = Visibility.Hidden;
            quantity.Visibility = Visibility.Hidden;
            totalQuantity.Visibility = Visibility.Hidden;
            price.Visibility = Visibility.Hidden;
            description.Visibility = Visibility.Hidden;
            allergenProducts.Visibility = Visibility.Hidden;
            addAllergen.Visibility = Visibility.Hidden;
            allergens.Visibility = Visibility.Hidden;
            imagesProduct.Visibility = Visibility.Hidden;
            addPhoto.Visibility = Visibility.Hidden;
            addProduct.Visibility = Visibility.Hidden;
            editProduct.Visibility = Visibility.Hidden;
            delProduct.Visibility = Visibility.Hidden;
            modifyProduct.Visibility = Visibility.Hidden;
            addProductBtn.Visibility = Visibility.Hidden;
            editMenu.Visibility = Visibility.Hidden;
            delMenu.Visibility = Visibility.Hidden;
            addMenuBtn.Visibility = Visibility.Hidden;
            menuProducts.Visibility = Visibility.Hidden;
            productQuantity.Visibility = Visibility.Hidden;
            products.Visibility = Visibility.Hidden;
            addProdForMenu.Visibility = Visibility.Hidden;
            menuName.Visibility = Visibility.Hidden;
            categoryMenu.Visibility = Visibility.Hidden;
            descriptionMenu.Visibility = Visibility.Hidden;
            imagesMenu.Visibility = Visibility.Hidden;
            addPhotoMenu.Visibility = Visibility.Hidden;
            minusButton.Visibility = Visibility.Hidden;
        }

        private void MakeVisibleAddCommand(object sender, EventArgs e)
        {
            SetAllToHidden();
            numberOfProducts.Visibility = Visibility.Visible;
            addButton.Visibility = Visibility.Visible;
            adress.Visibility = Visibility.Visible;
            commandProducts.Visibility = Visibility.Visible;
            finishCommand.Visibility = Visibility.Visible;
            minusButton.Visibility = Visibility.Visible;
        }

        private void MakeVisibleViewCommand(object sender, EventArgs e)
        {
            SetAllToHidden();
            commandProductsClient.Visibility = Visibility.Visible;
        }

        private void MakeVisibleViewCommandAbleToCancel(object sender, EventArgs e)
        {
            MakeVisibleViewCommand(sender, e);
            cancelCommand.Visibility = Visibility.Visible;
        }

        private void MakeVisibleUserOptions(object sender, EventArgs e)
        {
            if (Logger.UserType == Logger.User.CLIENT)
                clientOptions.Visibility = Visibility.Visible;
            else
                 if (Logger.UserType == Logger.User.EMPLOYEE)
                employeeOptions.Visibility = Visibility.Visible;
        }

        private void MakeVisibleEmployeeActiveCommands(object sender, EventArgs e)
        {
            SetAllToHidden();
            employeeCommands.Visibility = Visibility.Visible;
            statusCommand.Visibility = Visibility.Visible;
            modifyStatus.Visibility = Visibility.Visible;
        }

        private void MakeVisibleEmployeeCommands(object sender, EventArgs e)
        {
            SetAllToHidden();
            employeeCommands.Visibility = Visibility.Visible;
        }

        private void MakeVisibleProductsStock(object sender, EventArgs e)
        {
            SetAllToHidden();
            productsStock.Visibility = Visibility.Visible;
        }

        private void MakeVisibleAddCategory(object sender, EventArgs e)
        {
            SetAllToHidden();
            addCategory.Visibility = Visibility.Visible;
            categoryText.Visibility = Visibility.Visible;
        }

        private void MakeVisibleModifyCategory(object sender, EventArgs e)
        {
            SetAllToHidden();
            modifyCategory.Visibility = Visibility.Visible;
        }

        private void MakeVisibleButtonProductModifier(object sender, EventArgs e)
        {
            SetAllToHidden();
            editProduct.Visibility = Visibility.Visible;
            delProduct.Visibility = Visibility.Visible;
            addProductBtn.Visibility = Visibility.Visible;
        }

        private void MakeVisibleAddProduct(object sender, EventArgs e)
        {
            SetAllToHidden();
            prodName.Visibility = Visibility.Visible;
            category.Visibility = Visibility.Visible;
            quantity.Visibility = Visibility.Visible;
            totalQuantity.Visibility = Visibility.Visible;
            price.Visibility = Visibility.Visible;
            description.Visibility = Visibility.Visible;
            allergenProducts.Visibility = Visibility.Visible;
            addAllergen.Visibility = Visibility.Visible;
            allergens.Visibility = Visibility.Visible;
            imagesProduct.Visibility = Visibility.Visible;
            addPhoto.Visibility = Visibility.Visible;
            addProduct.Visibility = Visibility.Visible;
        }

        private void MakeVisibleEditProduct(object sender, EventArgs e)
        {
            MakeVisibleAddProduct(sender, e);
            editProduct.Visibility = Visibility.Visible;
            modifyProduct.Visibility = Visibility.Visible;
        }

        private void MakeVisibleButtonMenuModifier(object sender, EventArgs e)
        {
            SetAllToHidden();
            editMenu.Visibility = Visibility.Visible;
            delMenu.Visibility = Visibility.Visible;
            addMenuBtn.Visibility = Visibility.Visible;
        }

        private void MakeVisibleAddMenu(object sender, EventArgs e)
        {
            SetAllToHidden();
            menuName.Visibility = Visibility.Visible;
            categoryMenu.Visibility = Visibility.Visible;
            descriptionMenu.Visibility = Visibility.Visible;
            imagesMenu.Visibility = Visibility.Visible;
            addPhotoMenu.Visibility = Visibility.Visible;
            addProduct.Visibility = Visibility.Visible;
            menuProducts.Visibility = Visibility.Visible;
            productQuantity.Visibility = Visibility.Visible;
            products.Visibility = Visibility.Visible;
            addProdForMenu.Visibility = Visibility.Visible;
        }

        private void MakeVisibleEditMenu(object sender, EventArgs e)
        {
            MakeVisibleAddMenu(sender, e);
            editMenu.Visibility = Visibility.Visible;
            modifyProduct.Visibility = Visibility.Visible;
        }

        public HomePage()
        {
            InitializeComponent();
        }
    }
}
