USE [Restaurant]
GO
/****** Object:  StoredProcedure [dbo].[AddImageForMenu]    Script Date: 5/13/2020 11:28:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[AddImageForMenu](@id_menu int,
@poza nchar(150))
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO poza_meniu
	VALUES (@id_menu, @poza)
END
