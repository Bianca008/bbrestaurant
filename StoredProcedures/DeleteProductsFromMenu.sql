SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE DeleteProductsFromMenu(@id_meniu int)
AS
BEGIN
	SET NOCOUNT ON;

    DELETE continut_meniu
	WHERE id_meniu=@id_meniu

END
GO
