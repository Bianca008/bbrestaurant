USE [Restaurant]
GO
/****** Object:  StoredProcedure [dbo].[UpdateCommandStatus]    Script Date: 5/21/2020 6:32:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[UpdateCommandStatus](@cod nchar(4), @status nchar(40))
AS
BEGIN
	SET NOCOUNT ON;

    UPDATE comanda 
	SET status=@status
	WHERE cod=@cod

END
