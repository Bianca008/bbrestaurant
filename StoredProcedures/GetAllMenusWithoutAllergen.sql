USE [Restaurant]
GO
/****** Object:  StoredProcedure [dbo].[GetAllMenusWithoutAllergen]    Script Date: 5/13/2020 8:17:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[GetAllMenusWithoutAllergen](@denumire nchar(20))
AS
BEGIN
	SET NOCOUNT ON;

    SELECT meniu.id_meniu, meniu.denumire, meniu.descriere
	FROM meniu
	WHERE meniu.id_meniu
	NOT IN
	( SELECT meniu.id_meniu
	FROM meniu
	INNER JOIN continut_meniu ON continut_meniu.id_meniu=meniu.id_meniu
	INNER JOIN preparat ON preparat.id_preparat=continut_meniu.id_preparat
	INNER JOIN alergeni_preparat ON alergeni_preparat.id_preparat=preparat.id_preparat
	INNER JOIN alergen ON alergen.id_alergen=alergeni_preparat.id_alergen
	WHERE alergen.denumire=@denumire)

END
