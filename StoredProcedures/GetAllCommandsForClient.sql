USE [Restaurant]
GO
/****** Object:  StoredProcedure [dbo].[GetAllCommandsForClient]    Script Date: 5/27/2020 10:08:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[GetAllCommandsForClient](@id_client int)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT comanda.id_comanda, comanda.id_client, comanda.status, comanda.cod, comanda.data, comanda.durata_livrare, comanda.adresa
	FROM comanda WHERE comanda.id_client=@id_client
	ORDER BY comanda.data DESC
END
