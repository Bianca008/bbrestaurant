USE [Restaurant]
GO
/****** Object:  StoredProcedure [dbo].[MenuDisponibility]    Script Date: 5/14/2020 12:35:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[MenuDisponibility](@id_meniu int)

AS
BEGIN
	SET NOCOUNT ON;

    SELECT 1 FROM preparat
	INNER JOIN continut_meniu ON continut_meniu.id_preparat=preparat.id_preparat
	INNER JOIN meniu ON meniu.id_meniu=continut_meniu.id_meniu
	WHERE meniu.id_meniu=@id_meniu AND cantitate_totala<preparat.cantitate
END
