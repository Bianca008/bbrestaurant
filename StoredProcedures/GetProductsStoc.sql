SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE GetProductsStoc(@cantitate int)
AS
BEGIN
	SET NOCOUNT ON;

    SELECT denumire, cantitate_totala FROM preparat
	WHERE cantitate_totala<=@cantitate

END
GO
