SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE AddPhotoForMenuProd(@denumire nchar(40), @poza nchar(150))
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @id_meniu int;
	SET @id_meniu = (SELECT id_meniu FROM meniu WHERE denumire=@denumire);

	INSERT INTO poza_meniu
	VALUES (@id_meniu, @poza)

END
GO
