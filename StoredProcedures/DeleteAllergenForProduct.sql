SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE DeleteAllergenForProduct(@id_preparat int)
AS
BEGIN
	SET NOCOUNT ON;

    DELETE alergeni_preparat
	WHERE id_preparat=@id_preparat

END
GO
