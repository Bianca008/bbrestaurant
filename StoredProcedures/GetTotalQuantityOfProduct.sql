SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE GetTotalQuantityOfProduct(@denumire nchar(40))
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT cantitate_totala FROM preparat
	WHERE denumire=@denumire

END
GO
