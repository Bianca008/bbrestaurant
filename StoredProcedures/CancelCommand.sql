USE [Restaurant]
GO
/****** Object:  StoredProcedure [dbo].[CancelCommand]    Script Date: 5/19/2020 6:06:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[CancelCommand](@id_comanda int)
AS
BEGIN

	SET NOCOUNT ON;

    update comanda
	set status='anulata'
	where id_comanda=@id_comanda

END
