USE [Restaurant]
GO
/****** Object:  StoredProcedure [dbo].[GetQuantityAndPricePerMenu]    Script Date: 5/14/2020 12:33:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[GetQuantityAndPricePerMenu](@id_meniu int)
AS
BEGIN
	SET NOCOUNT ON;

    SELECT SUM(continut_meniu.cantitate), SUM(pret*continut_meniu.cantitate) FROM preparat
	INNER JOIN continut_meniu ON continut_meniu.id_preparat=preparat.id_preparat
	INNER JOIN meniu ON meniu.id_meniu=continut_meniu.id_meniu
	WHERE meniu.id_meniu=@id_meniu
END
