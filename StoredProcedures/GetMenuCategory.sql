SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE GetMenuCategory(@id_meniu int)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT tip FROM categorie_preparat
	INNER JOIN meniu ON meniu.id_categorie=categorie_preparat.id_categorie
	WHERE id_meniu=@id_meniu

END
GO
