SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE AddProductToCommand(@id_comanda int,
@id_preparat int,
@cantitate int)
AS
BEGIN
	SET NOCOUNT ON;

    INSERT INTO lista_preparate_comanda VALUES (@id_comanda, @id_preparat, @cantitate)
END
GO
