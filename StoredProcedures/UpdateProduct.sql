SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE UpdateProduct(
      @id_preparat int,
	  @denumire nchar(40),
      @pret real,
      @cantitate int,
      @cantitate_totala int,
      @id_categorie int,
      @descriere nchar(200))
AS
BEGIN
	SET NOCOUNT ON;

    UPDATE preparat
	SET denumire=@denumire, pret=@pret, cantitate=@cantitate,
	cantitate_totala=@cantitate_totala, id_categorie=@id_categorie,
	descriere=@descriere
	WHERE id_preparat=@id_preparat

END
GO
