USE [Restaurant]
GO
/****** Object:  StoredProcedure [dbo].[AddCommand]    Script Date: 5/16/2020 7:47:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[AddCommand](@id_client int, 
@cod nchar(4),
@data smalldatetime,
@durata_livrare int,
@adresa nchar(300))
AS
BEGIN
	SET NOCOUNT ON;

    INSERT INTO comanda
	VALUES
	(@id_client, 'inregistrata', @cod,
	 @data, @durata_livrare, @adresa)

END
