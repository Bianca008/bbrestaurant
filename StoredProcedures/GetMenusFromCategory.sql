SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE GetMenusFromCategory(@tip nchar(20))
AS
BEGIN
	SET NOCOUNT ON;

    SELECT id_meniu, denumire, descriere
	FROM meniu
	INNER JOIN categorie_preparat 
	ON meniu.id_categorie=categorie_preparat.id_categorie
	WHERE tip=@tip
END
GO
