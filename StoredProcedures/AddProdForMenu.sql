SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE AddProdForMenu(@denumire_preparat nchar(40),
@denumire_meniu nchar(40),
@cantitate int)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @id_preparat int;
	SET @id_preparat = (SELECT id_preparat FROM preparat WHERE denumire=@denumire_preparat);

    DECLARE @id_meniu int;
	SET @id_meniu = (SELECT id_meniu FROM meniu WHERE denumire=@denumire_meniu);

	INSERT INTO continut_meniu
	VALUES (@id_preparat, @id_meniu, @cantitate)

END
GO
