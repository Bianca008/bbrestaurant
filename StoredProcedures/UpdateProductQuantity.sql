USE [Restaurant]
GO
/****** Object:  StoredProcedure [dbo].[UpdateProductQuantity]    Script Date: 5/20/2020 12:11:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[UpdateProductQuantity](@denumire nchar(40), @cantitate int)
AS
BEGIN
	SET NOCOUNT ON;

    UPDATE preparat 
	SET cantitate_totala = (cantitate_totala-@cantitate)
	WHERE preparat.denumire=@denumire

END
