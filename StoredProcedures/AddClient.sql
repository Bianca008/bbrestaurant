SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE AddClient
(@nume nchar(50),
@prenume nchar(70),
@telefon nchar(10),
@email nchar(200),
@parola nchar(200))
AS
BEGIN
	SET NOCOUNT ON;

    INSERT INTO client
	VALUES (@nume, @prenume, @telefon, @email, @parola)
END
GO
