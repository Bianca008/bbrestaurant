SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE GetCommandId(@cod nchar(4))
AS
BEGIN
	SET NOCOUNT ON;

    SELECT id_comanda FROM comanda 
	WHERE cod=@cod
END
GO
