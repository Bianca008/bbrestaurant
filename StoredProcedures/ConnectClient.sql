USE [Restaurant]
GO
/****** Object:  StoredProcedure [dbo].[ConnectClient]    Script Date: 5/19/2020 2:50:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[ConnectClient](@email nchar(200),
@parola nchar(200))
AS
BEGIN
	SET NOCOUNT ON;

    SELECT 1 FROM client
	WHERE email=@email COLLATE SQL_Latin1_General_CP1_CS_AS AND parola=@parola COLLATE SQL_Latin1_General_CP1_CS_AS
END
