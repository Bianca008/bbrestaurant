SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE UpdatePhoto(@new_photo nchar(300), @old_photo nchar(300))
AS
BEGIN
	SET NOCOUNT ON;

    UPDATE poza 
	SET poza=@new_photo
	WHERE poza=@old_photo

END
GO
