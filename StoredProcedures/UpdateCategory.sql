SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE UpdateCategory(@noul_tip nchar(20), @vechiul_tip nchar(20))
AS
BEGIN
	
	SET NOCOUNT ON;

    UPDATE categorie_preparat
	SET tip=@noul_tip
	WHERE tip=@vechiul_tip

END
GO
