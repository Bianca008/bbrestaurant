USE [Restaurant]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[AddProduct](
	  @denumire nchar(40),
      @pret real,
      @cantitate int,
      @cantitate_totala int,
      @id_categorie int,
      @descriere nchar(200))
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [Restaurant].[dbo].[preparat](
      [denumire] 
      ,[pret]
      ,[cantitate]
      ,[cantitate_totala]
      ,[id_categorie]
      ,[descriere]
	)
	VALUES
	(
	  @denumire,
      @pret,
      @cantitate,
      @cantitate_totala,
      @id_categorie,
      @descriere)
END
