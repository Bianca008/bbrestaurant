SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE GetPhotosOfProduct(@id_preparat int)

AS
BEGIN
	SET NOCOUNT ON;
	SELECT poza FROM poza INNER JOIN preparat ON poza.id_preparat = preparat.id_preparat WHERE preparat.id_preparat = @id_preparat;
END
GO
