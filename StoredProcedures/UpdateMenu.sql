SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE UpdateMenu(
@id_meniu int,
@denumire nchar(40), 
@id_categorie int,
@descriere nchar(200))
AS
BEGIN
	SET NOCOUNT ON;

    UPDATE meniu
	SET denumire=@denumire, id_categorie=@id_categorie, descriere=@descriere
	WHERE id_meniu=@id_meniu

END
GO
