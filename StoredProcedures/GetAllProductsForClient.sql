USE [Restaurant]
GO
/****** Object:  StoredProcedure [dbo].[GetAllProductsForClient]    Script Date: 5/19/2020 11:09:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[GetAllProductsForClient](@id_comanda int)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT preparat.denumire, preparat.pret*preparat.cantitate/100*lista_preparate_comanda.cantitate, lista_preparate_comanda.cantitate
	FROM preparat 
	INNER JOIN lista_preparate_comanda ON lista_preparate_comanda.id_preparat=preparat.id_preparat
	WHERE lista_preparate_comanda.id_comanda=@id_comanda

END
