SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE AddPhotoToProduct(@denumire nchar(40), @poza nchar(300))
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @id_preparat int;
	SET @id_preparat = (SELECT id_preparat FROM preparat WHERE denumire=@denumire)

    INSERT INTO poza
	VALUES (@id_preparat, @poza)
END
GO
