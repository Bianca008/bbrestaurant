SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE AddAlergen(@denumire nchar(20))
AS
BEGIN
	SET NOCOUNT ON;

    INSERT INTO [Restaurant].[dbo].[alergen]([denumire])
	VALUES (@denumire)
END
GO
