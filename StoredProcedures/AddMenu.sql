SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE AddMenu(@denumire nchar(40),
@id_categorie int, 
@descriere nchar(200))
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO meniu
	VALUES
	(@denumire, @id_categorie, @descriere)
END
GO
