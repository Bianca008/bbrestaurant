USE [Restaurant]
GO
/****** Object:  StoredProcedure [dbo].[GetAllProductsWithoutAllergen]    Script Date: 5/13/2020 8:26:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[GetAllProductsWithoutAllergen](@denumire nchar(20))
AS
BEGIN
	SET NOCOUNT ON;

    SELECT preparat.id_preparat, preparat.denumire, pret, cantitate, descriere
	FROM preparat 
	WHERE preparat.id_preparat
	NOT IN
	(SELECT preparat.id_preparat
	FROM preparat 
	INNER JOIN alergeni_preparat ON preparat.id_preparat=alergeni_preparat.id_preparat
	INNER JOIN alergen ON alergen.id_alergen=alergeni_preparat.id_alergen
	WHERE alergen.denumire=@denumire)

END
