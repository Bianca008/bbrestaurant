SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE AddAllergenForProduct(@denumire_produs char(40), 
									   @denumire_alergen nchar(20))
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @id_preparat int;
	SET @id_preparat = (SELECT id_preparat FROM preparat WHERE denumire=@denumire_produs)

	DECLARE @id_alergen int;
	SET @id_alergen = (SELECT id_alergen FROM alergen WHERE denumire=@denumire_alergen)

	INSERT INTO alergeni_preparat
	VALUES (@id_preparat, @id_alergen)

END
GO
