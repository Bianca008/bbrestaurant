USE [Restaurant]
GO
/****** Object:  StoredProcedure [dbo].[GetAllActiveCommandsForEmployee]    Script Date: 5/20/2020 6:58:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[GetAllActiveCommandsForEmployee]
AS
BEGIN
	SET NOCOUNT ON;

     SELECT id_comanda, comanda.id_client, 
	comanda.status, comanda.cod, 
	comanda.data, comanda.adresa,
	client.nume, client.prenume, client.telefon 
	FROM comanda
	INNER JOIN client on client.id_client=comanda.id_client
	WHERE comanda.status!='livrata' AND comanda.status!='anulata'
	ORDER BY comanda.data DESC

END
