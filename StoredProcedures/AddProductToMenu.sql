SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE AddProductToMenu(@id_preparat int,
@id_meniu int)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO continut_meniu
	VALUES (@id_preparat, @id_meniu)
   
END
GO
