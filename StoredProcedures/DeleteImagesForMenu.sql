SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE DeleteImagesForMenu(@id_meniu int)
AS
BEGIN
	SET NOCOUNT ON;

    DELETE poza_meniu 
	WHERE id_meniu=@id_meniu

END
GO
