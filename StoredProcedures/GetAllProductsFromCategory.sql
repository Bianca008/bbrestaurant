USE [Restaurant]
GO
/****** Object:  StoredProcedure [dbo].[GetAllProductsFromCategory]    Script Date: 5/14/2020 1:46:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[GetAllProductsFromCategory](@tip nchar(20))
AS
BEGIN
	SET NOCOUNT ON;

	SELECT id_preparat, denumire, pret*cantitate/100, cantitate, descriere
	FROM preparat
	INNER JOIN categorie_preparat 
	ON preparat.id_categorie=categorie_preparat.id_categorie
	WHERE tip=@tip
END
