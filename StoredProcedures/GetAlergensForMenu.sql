SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE GetAlergensForMenu(@id_meniu int)
AS
BEGIN
	SET NOCOUNT ON;

    SELECT DISTINCT alergen.denumire FROM alergen
	INNER JOIN alergeni_preparat ON alergeni_preparat.id_alergen=alergen.id_alergen
	INNER JOIN preparat ON preparat.id_preparat=alergeni_preparat.id_preparat
	INNER JOIN continut_meniu ON continut_meniu.id_preparat=preparat.id_preparat
	INNER JOIN meniu ON meniu.id_meniu=continut_meniu.id_meniu
	WHERE meniu.id_meniu=@id_meniu
END
GO
