USE [Restaurant]
GO
/****** Object:  StoredProcedure [dbo].[GetDisponibilityOfProduct]    Script Date: 5/13/2020 2:07:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[GetDisponibilityOfProduct](@id_preparat int)
AS
BEGIN
	SET NOCOUNT ON;

    SELECT 1 FROM preparat
	WHERE preparat.id_preparat=@id_preparat AND cantitate_totala<cantitate
END
