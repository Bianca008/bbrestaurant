SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE GetProductCategory(@id_produs int)
AS
BEGIN
	SET NOCOUNT ON;

    SELECT tip FROM categorie_preparat
	INNER JOIN preparat ON categorie_preparat.id_categorie=preparat.id_categorie
	WHERE preparat.id_preparat=@id_produs

END
GO
