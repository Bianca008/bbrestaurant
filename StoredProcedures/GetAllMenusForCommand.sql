USE [Restaurant]
GO
/****** Object:  StoredProcedure [dbo].[GetAllMenusForCommand]    Script Date: 5/19/2020 11:20:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[GetAllMenusForCommand](@id_comanda int)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT meniu.id_meniu, meniu.denumire, lista_meniuri_comanda.cantitate 
	FROM meniu 
	INNER JOIN lista_meniuri_comanda ON lista_meniuri_comanda.id_meniu=meniu.id_meniu
	WHERE lista_meniuri_comanda.id_comanda=@id_comanda

END
