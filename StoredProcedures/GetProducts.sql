USE [Restaurant]
GO
/****** Object:  StoredProcedure [dbo].[GetProducts]    Script Date: 5/14/2020 1:45:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[GetProducts]
AS
BEGIN
	SET NOCOUNT ON;

    SELECT id_preparat, denumire, pret*cantitate/100, cantitate, descriere
	FROM preparat
END
