-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE GetAlergensOfProduct(@id_preparat nchar(40))
AS
BEGIN

	SET NOCOUNT ON;

    SELECT alergen.denumire 
	FROM alergen 
	INNER JOIN alergeni_preparat 
	ON alergen.id_alergen = alergeni_preparat.id_alergen
	INNER JOIN preparat
	ON preparat.id_preparat = alergeni_preparat.id_preparat
	WHERE alergeni_preparat.id_preparat = @id_preparat
END
GO
