SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE GetQuantitiesOfMenuProducts(@denumire nchar(40))
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT preparat.denumire, continut_meniu.cantitate
	FROM meniu 
	INNER JOIN continut_meniu ON continut_meniu.id_meniu=meniu.id_meniu
	INNER JOIN preparat ON preparat.id_preparat=continut_meniu.id_preparat
	WHERE meniu.denumire=@denumire

END
GO
