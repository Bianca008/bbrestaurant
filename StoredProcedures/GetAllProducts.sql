SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE GetAllProducts
AS
BEGIN
	SET NOCOUNT ON;
    
    SELECT id_preparat, denumire, pret, cantitate, fotografie, descriere 
	FROM preparat

END