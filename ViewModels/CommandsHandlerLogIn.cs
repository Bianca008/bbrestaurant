﻿using BBRestaurant.Models.BusinessLogicLayer;
using BBRestaurant.Models.EntityLayer;
using BBRestaurant.View;
using System.Windows.Controls;
using System.Windows.Input;

namespace BBRestaurant.ViewModels
{
    class CommandsHandlerLogIn
    {
        ICommand m_newAccount;
        ICommand m_logIn;
        ClientBll m_clientBll;

        public CommandsHandlerLogIn()
        {
            m_clientBll = new ClientBll();
        }

        private void Start(object parameter)
        {
            CreateAccountPage newAccount = new CreateAccountPage();
            newAccount.Show();
        }

        public ICommand CreateAccount
        {
            get
            {
                if (m_newAccount == null)
                    m_newAccount = new RelayCommand<object>(Start);
                return m_newAccount;
            }
        }

        public ICommand LogIn
        {
            get
            {
                if (m_logIn == null)
                    m_logIn = new RelayCommand<Logger>(m_clientBll.Connect);
                return m_logIn;
            }
        }
    }
}
