﻿using BBRestaurant.Models;
using BBRestaurant.Models.BusinessLogicLayer;
using BBRestaurant.Models.EntityLayer;
using BBRestaurant.View;
using Microsoft.Win32;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace BBRestaurant.ViewModels
{
    class CommandsHandlerHomePage : BasePropertyChanged
    {
        CategoryBll m_categoryBll;
        ProductToShowBll m_productToShowBll;
        ClientProductBll m_clientProductBll;
        CommandBll m_commandBll;
        ProductStockBll m_productStockBll;
        ProductBll m_productBll;
        MenuBll m_menuBll;

        string m_category;
        string m_modifiedCategory;

        Product m_newProduct;
        Models.EntityLayer.Menu m_newMenu;

        ICommand m_searchMenuAfterAllergen;
        ICommand m_searchMenuAfterWord;
        ICommand m_productsWithoutAllergen;
        ICommand m_startLogger;
        ICommand m_addItem;
        ICommand m_finishCommand;
        ICommand m_activeCommands;
        ICommand m_commands;
        ICommand m_cancelCommand;
        ICommand m_viewAllCommands;
        ICommand m_viewAllActiveCommands;
        ICommand m_modify;
        ICommand m_viewStock;
        ICommand m_addCategory;
        ICommand m_modifyCategory;
        ICommand m_deleteCategory;
        ICommand m_newAllergen;
        ICommand m_addProductPhoto;
        ICommand m_modifyProduct;
        ICommand m_addProduct;
        ICommand m_deleteProduct;
        ICommand m_clear;
        ICommand m_updateProduct;
        ICommand m_addProductToMenu;
        ICommand m_addPhotoToMenu;
        ICommand m_modifyMenu;
        ICommand m_deleteProductFromCommand;

        public CommandsHandlerHomePage()
        {
            m_categoryBll = new CategoryBll();
            m_clientProductBll = new ClientProductBll();
            m_productToShowBll = new ProductToShowBll();
            m_commandBll = new CommandBll();
            m_productStockBll = new ProductStockBll();
            m_productBll = new ProductBll();
            m_menuBll = new MenuBll();
            ProductAllergens = new ObservableCollection<string>();
            ProductImages = new ObservableCollection<string>();
            MenuQuantityOfProfuct = new ObservableCollection<ProductStock>();
            m_newProduct = new Product();
            m_newMenu = new Models.EntityLayer.Menu();
            CategoryList = m_categoryBll.GetAllCategories();
            ProductsListToShow = m_productToShowBll.GetAllProducts();
            ClientCommandsList = m_commandBll.GetAllClientCommands();
            m_category = "TOATE";
        }

        public string ModifiedCategory
        {
            get => m_modifiedCategory;

            set
            {
                m_modifiedCategory = value;
                NotifyPropertyChanged("ModifiedCatgeory");
            }
        }

        public ObservableCollection<ProductToShow> AllProdList
        {
            get => m_productToShowBll.GetAllProducts();
        }

        public ObservableCollection<string> ProductImages
        {
            get => m_newProduct.Images;
            set
            {
                var images = value as ObservableCollection<string>;
                if (images.Count != 0)
                    m_newProduct.Images = images;
                NotifyPropertyChanged("ProductImages");
            }

        }

        public ObservableCollection<Category> CategoryList
        {
            get
            {
                return m_categoryBll.CategoryList;
            }
            set
            {
                m_categoryBll.CategoryList = value;
            }
        }

        public ObservableCollection<ProductToShow> ProductsListToShow
        {
            get
            {
                return m_productToShowBll.ProductsList;
            }

            set
            {
                m_productToShowBll.ProductsList = value;
                NotifyPropertyChanged("ProductsListToShow");
            }
        }

        public ObservableCollection<ClientProduct> ClientProductsList
        {
            get
            {
                return m_clientProductBll.Products;
            }

            set
            {
                m_clientProductBll.Products = value;
                NotifyPropertyChanged("ClientProductsList");
            }
        }

        public ObservableCollection<Command> ClientCommandsList
        {
            get
            {
                return m_commandBll.ClientCommandList;
            }

            set
            {
                m_commandBll.ClientCommandList = value;
                NotifyPropertyChanged("ClientCommandsList");
            }
        }

        public ObservableCollection<Command> EmployeeCommandsList
        {
            get
            {
                return m_commandBll.EmployeeCommandsList;
            }

            set
            {
                m_commandBll.EmployeeCommandsList = value;
                NotifyPropertyChanged("EmployeeCommandsList");
            }
        }

        public ObservableCollection<ProductStock> ProductsStockList
        {
            get
            {
                return m_productStockBll.ProductsStock;
            }

            set
            {
                m_productStockBll.ProductsStock = value;
                NotifyPropertyChanged("ProductsStockList");
            }
        }

        public ObservableCollection<string> ProductAllergens
        {
            get => m_newProduct.Allergens;
            set
            {
                var allergens = value as ObservableCollection<string>;
                if (allergens.Count != 0)
                    m_newProduct.Allergens = allergens;
                NotifyPropertyChanged("ProductAllergens");
            }
        }

        public List<MenuItem> MenuCreator
        {
            get
            {
                int m_indexCategory = 0;
                List<MenuItem> items = new List<MenuItem>();
                MenuItem menu = new MenuItem();
                menu.Header = "   MENIURI  ";
                items.Add(menu);

                for (int index = 0; index < CategoryList.Count; ++index)
                {
                    MenuItem item = new MenuItem();
                    string intermediar = CategoryList[m_indexCategory++].CategoryName.ToUpper();
                    intermediar = intermediar.Replace("   ", " ");
                    item.Header = intermediar;
                    items.Add(item);
                }
                MenuItem all = new MenuItem();
                all.Header = "  TOATE  ";
                items.Add(all);

                return items;
            }
        }

        private void SearchCategory(object parameter)
        {
            var menuItem = parameter as MenuItem;
            string categoryName = menuItem.Header as string;
            m_category = categoryName;

            ProductsListToShow = m_productToShowBll.GetAllProductsFromCategory(categoryName);
        }

        public ICommand SearchMenu
        {
            get
            {
                if (m_searchMenuAfterAllergen == null)
                    m_searchMenuAfterAllergen = new RelayCommand<object>(SearchCategory);
                return m_searchMenuAfterAllergen;
            }
        }

        private void ProdsWithoutAllergen(object parameter)
        {
            var allergenCombo = (ComboBox)parameter;

            if (allergenCombo.SelectedItem != null)
            {
                string allergen = allergenCombo.SelectedItem.ToString();
                allergen = allergen.Substring(allergen.IndexOf(" ") + 1);

                if (m_category.IndexOf("MENIU") != -1)
                {
                    ProductsListToShow = m_productToShowBll.GetAllMenusWithoutAllergen(allergen);
                    return;
                }

                if (m_category.IndexOf("TOATE") != -1)
                {
                    ProductsListToShow = m_productToShowBll.GetAllProductsWithoutAllergen(allergen);

                    ObservableCollection<ProductToShow> intermediar = m_productToShowBll.
                        GetAllMenusWithoutAllergen(allergen);

                    for (int index = 0; index < intermediar.Count; ++index)
                        ProductsListToShow.Add(intermediar[index]);

                    return;
                }

                ProductsListToShow = m_productToShowBll.GetAllProductsWithoutAllergen(m_category, allergen);
            }
        }

        public ICommand ProductsWithoutAllergen
        {
            get
            {
                if (m_productsWithoutAllergen == null)
                    m_productsWithoutAllergen = new RelayCommand<object>(ProdsWithoutAllergen);
                return m_productsWithoutAllergen;
            }
        }

        private void ProductsContainingAli(object parameter)
        {
            var textBox = parameter as TextBox;
            string aliment = textBox.Text;

            ProductsListToShow = m_productToShowBll.SearchProductsFromWord(m_category, aliment);
        }

        public ICommand ProductsContainingAliment
        {
            get
            {
                if (m_searchMenuAfterWord == null)
                    m_searchMenuAfterWord = new RelayCommand<object>(ProductsContainingAli);
                return m_searchMenuAfterWord;
            }
        }

        private void Start(object parameter)
        {
            LogInPage logger = new LogInPage();
            if (Logger.UserType == Logger.User.NOT_CONNECTED)
                logger.Show();
            else
                logger.Close();
        }

        public ICommand StartLogger
        {
            get
            {
                if (m_startLogger == null)
                    m_startLogger = new RelayCommand<object>(Start);
                return m_startLogger;
            }
        }

        public ICommand AddItemToCommand
        {
            get
            {
                if (m_addItem == null)
                    m_addItem = new RelayCommand<ClientProduct>(m_clientProductBll.AddItemToCommand);
                return m_addItem;
            }
        }

        private void Finish(object parameter)
        {
            var textBox = parameter as TextBox;

            if (textBox.Text != "")
            {
                bool applyDiscount = m_commandBll.HasZCommandInTdays(m_clientProductBll.GetIdClient(Logger.Mail));
                MessageBox.Show("Pret total: " + PriceCalculator.GenerateDiscountsAndDelivery(ClientProductsList,
                    applyDiscount));
                m_clientProductBll.AddCommand(textBox.Text);
                textBox.Text = "";
            }
            else
                MessageBox.Show("Adresa de livrare nu a fost completata!");

            ProductsListToShow = m_productToShowBll.GetAllProductsFromCategory(m_category);
        }

        public ICommand FinishCommand
        {
            get
            {
                if (m_finishCommand == null)
                    m_finishCommand = new RelayCommand<object>(Finish);
                return m_finishCommand;
            }
        }

        private void GenerateActiveCommands(object parameter)
        {
            ClientCommandsList = m_commandBll.GetAllClientActiveCommands();
        }

        public ICommand ActiveCommands
        {
            get
            {
                if (m_activeCommands == null)
                    m_activeCommands = new RelayCommand<object>(GenerateActiveCommands);
                return m_activeCommands;
            }
        }

        private void GenerateCommands(object parameter)
        {
            ClientCommandsList = m_commandBll.GetAllClientCommands();
        }

        public ICommand Commands
        {
            get
            {
                if (m_commands == null)
                    m_commands = new RelayCommand<object>(GenerateCommands);
                return m_commands;
            }
        }

        private void Cancel(object parameter)
        {
            ListBox listBox = parameter as ListBox;
            var selectedItem = listBox.SelectedItem as Command;

            if (selectedItem != null)
            {
                int commandId = selectedItem.CommandId;

                m_commandBll.CancelCommand(commandId);
                ClientCommandsList = m_commandBll.GetAllClientActiveCommands();

                MessageBox.Show("Comanda a fost anulata cu scces!");
            }
        }

        public ICommand CancelCommand
        {
            get
            {
                if (m_cancelCommand == null)
                    m_cancelCommand = new RelayCommand<object>(Cancel);
                return m_cancelCommand;
            }
        }

        private void ViewAllCommands(object parmeter)
        {
            EmployeeCommandsList = m_commandBll.GetAllEmployeeCommands();
        }

        public ICommand ViewCommands
        {
            get
            {
                if (m_viewAllCommands == null)
                    m_viewAllCommands = new RelayCommand<object>(ViewAllCommands);
                return m_viewAllCommands;
            }
        }

        private void ViewAllActiveCommands(object parmeter)
        {
            EmployeeCommandsList = m_commandBll.GetAllActiveCommandsForEmployee();
        }

        public ICommand ViewActiveCommands
        {
            get
            {
                if (m_viewAllActiveCommands == null)
                    m_viewAllActiveCommands = new RelayCommand<object>(ViewAllActiveCommands);
                return m_viewAllActiveCommands;
            }
        }

        public ICommand ModifyStatus
        {
            get
            {
                if (m_modify == null)
                    m_modify = new RelayCommand<Status>(m_commandBll.UpdateStatus);
                return m_modify;
            }
        }

        private void ViewStockEmployee(object parameter)
        {
            ProductsStockList = m_productStockBll.GetAllProductsStock();
        }

        public ICommand ViewStock
        {
            get
            {
                if (m_viewStock == null)
                    m_viewStock = new RelayCommand<object>(ViewStockEmployee);
                return m_viewStock;
            }
        }

        private void AddCategory(object parameter)
        {
            var category = parameter as TextBox;

            string cat = category.Text;
            if (cat != "")
            {
                m_categoryBll.AddCategory(cat);
                CategoryList = m_categoryBll.GetAllCategories();
                NotifyPropertyChanged("MenuCreator");
                NotifyPropertyChanged("CategoryList");
                category.Text = "";
            }
        }

        public ICommand AddNewCategory
        {
            get
            {
                if (m_addCategory == null)
                    m_addCategory = new RelayCommand<object>(AddCategory);
                return m_addCategory;
            }
        }

        private void ModifyCat(ListBox parameter)
        {
            Category oldCategory = parameter.SelectedItem as Category;

            if (oldCategory != null && ModifiedCategory != "")
            {
                string oldCat = oldCategory.CategoryName;
                m_categoryBll.UpdateCategory(ModifiedCategory, oldCat);

                CategoryList = m_categoryBll.GetAllCategories();
                NotifyPropertyChanged("CategoryList");
                NotifyPropertyChanged("MenuCreator");
            }
        }

        public ICommand ModifyCategory
        {
            get
            {
                if (m_modifyCategory == null)
                    m_modifyCategory = new RelayCommand<ListBox>(ModifyCat);
                return m_modifyCategory;
            }
        }

        private void DeleteCat(ListBox parameter)
        {
            Category category = parameter.SelectedItem as Category;

            if (category != null && ModifiedCategory != "")
            {
                string cat = category.CategoryName;
                m_categoryBll.DeleteCategory(cat);

                CategoryList = m_categoryBll.GetAllCategories();
                ProductsListToShow = m_productToShowBll.GetAllProductsFromCategory(m_category);
                NotifyPropertyChanged("CategoryList");
                NotifyPropertyChanged("MenuCreator");
            }
        }

        public ICommand DeleteCategory
        {
            get
            {
                if (m_deleteCategory == null)
                    m_deleteCategory = new RelayCommand<ListBox>(DeleteCat);
                return m_deleteCategory;
            }
        }

        private void AddAllergen(ComboBox parameter)
        {
            ComboBoxItem allergenCombo = parameter.SelectedItem as ComboBoxItem;

            if (allergenCombo != null)
            {
                string allergen = allergenCombo.Content.ToString();
                if (ProductAllergens.IndexOf(allergen) == -1)
                {
                    ProductAllergens.Add(allergen);
                    NotifyPropertyChanged("ProductAllergens");
                }
            }
        }

        public ICommand AddNewAllergen
        {
            get
            {
                if (m_newAllergen == null)
                    m_newAllergen = new RelayCommand<ComboBox>(AddAllergen);
                return m_newAllergen;
            }
        }

        private void AddPhoto(object parameter)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";

            if (op.ShowDialog() == true)
            {
                ProductImages.Add(op.FileName);
                NotifyPropertyChanged("ProductImages");
            }
        }

        public ICommand AddPhotoProduct
        {
            get
            {
                if (m_addProductPhoto == null)
                    m_addProductPhoto = new RelayCommand<object>(AddPhoto);
                return m_addProductPhoto;
            }
        }

        public string ProductName
        {
            get => m_newProduct.Name;
            set
            {
                m_newProduct.Name = value;
                NotifyPropertyChanged("ProductName");
            }
        }

        public string ProductQuantity
        {
            get => m_newProduct.Quantity;
            set
            {
                m_newProduct.Quantity = value;
                NotifyPropertyChanged("ProductQuantity");
            }
        }

        public string ProductTotalQuantity
        {
            get => m_newProduct.TotalQuantity;
            set
            {
                m_newProduct.TotalQuantity = value;
                NotifyPropertyChanged("ProductTotalQuantity");
            }
        }

        public string ProductPrice
        {
            get => m_newProduct.Price;
            set
            {
                m_newProduct.Price = value;
                NotifyPropertyChanged("ProductPrice");
            }
        }

        public string ProductDescription
        {
            get => m_newProduct.Description;
            set
            {
                m_newProduct.Description = value;
                NotifyPropertyChanged("ProductDescription");
            }
        }

        public ProductToShow SelectedProductToModifiy
        {
            get;
            set;
        }

        public ProductToShow InitialProduct
        {
            get;
            set;
        }

        public Category ProductCategory
        {
            get => m_newProduct.Category;
            set
            {
                m_newProduct.Category = value;
                NotifyPropertyChanged("ProductCategory");
            }
        }

        private void ModifyMenu(object parameter)
        {
            InitialProduct = SelectedProductToModifiy;

            if (SelectedProductToModifiy != null && m_clientProductBll.IsProduct(SelectedProductToModifiy.Name) == false)
            {
                var par = SelectedProductToModifiy;
                MenuName = SelectedProductToModifiy.Name;
                MenuDescription = SelectedProductToModifiy.Description;

                MenuImages.Clear();
                foreach (var photo in SelectedProductToModifiy.PhotosList)
                    MenuImages.Add(photo);

                string catName = m_productToShowBll.GetCategoryOfMenu(SelectedProductToModifiy.ProductId);
                Category prodCategory = CategoryList.Single<Category>(obj => obj.
                                        CategoryName.IndexOf(catName) != -1);

                MenuQuantityOfProfuct.Clear();
                foreach (var quantity in SelectedProductToModifiy.Quantity)
                {
                    int g = int.Parse(quantity.Substring(0, quantity.IndexOf(" ") - 1));
                    string name = quantity.Substring(quantity.IndexOf(" ") + 1);
                    ProductStock prod = new ProductStock
                    {
                        Name = name,
                        TotalQuantity = g
                    };
                    MenuQuantityOfProfuct.Add(prod);
                }

                MenuCategory = prodCategory;
            }
        }

        private void ModifyProd(object parameter)
        {
            InitialProduct = SelectedProductToModifiy;

            if (SelectedProductToModifiy != null && m_clientProductBll.IsProduct(SelectedProductToModifiy.Name) == true)
            {
                var par = SelectedProductToModifiy;
                ProductName = SelectedProductToModifiy.Name;
                ProductQuantity = SelectedProductToModifiy.Quantity[0].ToString();
                ProductPrice = SelectedProductToModifiy.Price.ToString();
                ProductDescription = SelectedProductToModifiy.Description;
                ProductTotalQuantity = m_productToShowBll.GetQuantityOfProduct(ProductName).ToString();

                ProductAllergens.Clear();
                foreach (var allergen in SelectedProductToModifiy.AlergensList)
                    ProductAllergens.Add(allergen);

                ProductImages.Clear();
                foreach (var photo in SelectedProductToModifiy.PhotosList)
                    ProductImages.Add(photo);

                string catName = m_productToShowBll.GetCategoryOfProduct(SelectedProductToModifiy.ProductId);
                Category prodCategory = CategoryList.Single<Category>(obj => obj.
                                        CategoryName.IndexOf(catName) != -1);

                ProductCategory = prodCategory;
            }
        }

        public ICommand ModifyProduct
        {
            get
            {
                if (m_modifyProduct == null)
                    m_modifyProduct = new RelayCommand<object>(ModifyProd);
                return m_modifyProduct;
            }
        }

        public ICommand ModifyMenus
        {
            get
            {
                if (m_modifyMenu == null)
                    m_modifyMenu = new RelayCommand<object>(ModifyMenu);
                return m_modifyMenu;
            }
        }

        private void ClearNewProduct()
        {
            ProductName = "";
            ProductQuantity = "";
            ProductTotalQuantity = "";
            ProductDescription = "";
            ProductPrice = "";
            ProductCategory = null;
            ProductAllergens.Clear();
            ProductImages.Clear();
        }

        private void ClearMenu()
        {
            MenuName = "";
            MenuDescription = "";
            MenuCategory = null;
            MenuImages.Clear();
            NewQuantityForMenuProduct = "";
            MenuQuantityOfProfuct.Clear();
        }

        private void AddProduct(object parameter)
        {
            if (m_newProduct.Name != "")
            {
                var allProd = m_productToShowBll.GetAllProductsAndMenus();
                for (int index = 0; index < allProd.Count; ++index)
                {
                    string prodName = allProd[index].Name.Replace(" ", "").ToLower();
                    string newProdName = m_newProduct.Name.Replace(" ", "").ToLower();

                    if (prodName == newProdName)
                    {
                        MessageBox.Show("Exista deja un produs cu acest nume!");
                        return;
                    }
                }
            }

            if (m_newProduct.NotEmptyAndIncorrectFields() == true)
            {
                m_productBll.AddProduct(m_newProduct);
                ProductsListToShow = m_productToShowBll.GetAllProductsFromCategory(m_category);
                NotifyPropertyChanged("ProductsListToShow");

                MessageBox.Show("Produsul a fost adaugat cu succes!");

                ClearNewProduct();
            }
            else
            if (m_newMenu.NotEmptyAndIncorrectFields() == true)
            {
                m_menuBll.AddMenu(m_newMenu);
                ProductsListToShow = m_productToShowBll.GetAllProductsFromCategory(m_category);
                NotifyPropertyChanged("ProductsListToShow");

                MessageBox.Show("Meniul a fost adaugat cu succes!");

                ClearMenu();
            }
            else
                MessageBox.Show("Formularul nu a fost completat corect. Mai incearca o data!");
        }

        public ICommand AddNewProduct
        {
            get
            {
                if (m_addProduct == null)
                    m_addProduct = new RelayCommand<object>(AddProduct);
                return m_addProduct;
            }
        }

        private void DelProduct(object parameter)
        {
            if (SelectedProductToModifiy != null && m_clientProductBll.IsProduct(SelectedProductToModifiy.Name) == true)
            {
                m_productBll.DeleteProduct(SelectedProductToModifiy.ProductId);
                ProductsListToShow = m_productToShowBll.GetAllProductsFromCategory(m_category);
                NotifyPropertyChanged("ProductsListToShow");

                MessageBox.Show("Produsul a fost sters cu succes!");

                ClearNewProduct();
            }
            else
            if (SelectedProductToModifiy != null && m_clientProductBll.IsProduct(SelectedProductToModifiy.Name) == false)
            {
                m_menuBll.DeleteMenu(SelectedProductToModifiy.ProductId);
                ProductsListToShow = m_productToShowBll.GetAllProductsFromCategory(m_category);
                NotifyPropertyChanged("ProductsListToShow");

                MessageBox.Show("Meniul a fost sters cu succes!");

                ClearMenu();
            }
        }

        public ICommand DeleteProduct
        {
            get
            {
                if (m_deleteProduct == null)
                    m_deleteProduct = new RelayCommand<object>(DelProduct);
                return m_deleteProduct;
            }
        }

        private void ClearNewProd(object parameter)
        {
            ClearNewProduct();
        }

        public ICommand ClearFields
        {
            get
            {
                if (m_clear == null)
                    m_clear = new RelayCommand<object>(ClearNewProd);
                return m_clear;
            }
        }

        private void UpdateProd(object parameter)
        {
            if (InitialProduct != null && m_newProduct.NotEmptyAndIncorrectFields() == true)
            {
                m_productBll.UpdateProduct(InitialProduct, m_newProduct);
                ProductsListToShow = m_productToShowBll.GetAllProductsFromCategory(m_category);
                NotifyPropertyChanged("ProductsListToShow");

                MessageBox.Show("Produsul a fost modificat cu succes!");

                ClearNewProduct();
            }
            else
                if (InitialProduct != null && m_newMenu.NotEmptyAndIncorrectFields() == true)
            {
                m_menuBll.UpdateMenu(InitialProduct, m_newMenu);
                ProductsListToShow = m_productToShowBll.GetAllProductsFromCategory(m_category);
                NotifyPropertyChanged("ProductsListToShow");

                MessageBox.Show("Produsul a fost modificat cu succes!");

                ClearNewProduct();
            }
        }

        public ICommand UpdateProduct
        {
            get
            {
                if (m_updateProduct == null)
                    m_updateProduct = new RelayCommand<object>(UpdateProd);
                return m_updateProduct;
            }
        }

        public ObservableCollection<ProductStock> MenuQuantityOfProfuct
        {
            get => m_newMenu.QuantityAndName;
            set
            {
                ObservableCollection<ProductStock> productStock = value as ObservableCollection<ProductStock>;
                if (productStock.Count != 0)
                {
                    m_newMenu.QuantityAndName = productStock;
                    NotifyPropertyChanged("MenuQuantityOfProfuct");
                }
            }
        }

        public string MenuName
        {
            get => m_newMenu.Name;
            set
            {
                m_newMenu.Name = value;
                NotifyPropertyChanged("MenuName");
            }
        }

        public string MenuDescription
        {
            get => m_newMenu.Description;
            set
            {
                m_newMenu.Description = value;
                NotifyPropertyChanged("MenuDescription");
            }
        }

        string m_quantity;

        public string NewQuantityForMenuProduct
        {
            get => m_quantity;
            set
            {
                m_quantity = value;
                NotifyPropertyChanged("NewQuantityForMenuProduct");
            }
        }

        public ObservableCollection<string> MenuImages
        {
            get => m_newMenu.Images;
            set
            {
                var images = value as ObservableCollection<string>;
                if (images.Count != 0)
                    m_newMenu.Images = images;
                NotifyPropertyChanged("ProductImages");
            }

        }

        public ProductToShow ProductForMenu
        {
            get;
            set;
        }

        public Category MenuCategory
        {
            get => m_newMenu.Category;
            set
            {
                m_newMenu.Category = value;
                NotifyPropertyChanged("MenuCategory");
            }
        }

        private bool CheckIsPositiveInt(string value)
        {
            int result;

            if (int.TryParse(value, out result) && result > 0)
                return true;

            return false;
        }

        private void AddProdToMenu(object parameter)
        {
            if (ProductForMenu != null &&
                NewQuantityForMenuProduct != "" &&
                NewQuantityForMenuProduct != null &&
                CheckIsPositiveInt(NewQuantityForMenuProduct) == true)
            {
                for (int index = 0; index < MenuQuantityOfProfuct.Count; ++index)
                    if (ProductForMenu.Name == MenuQuantityOfProfuct[index].Name)
                    {
                        MenuQuantityOfProfuct[index].TotalQuantity += int.Parse(NewQuantityForMenuProduct);
                        return;
                    }

                MenuQuantityOfProfuct.Add(new ProductStock
                {
                    Name = ProductForMenu.Name,
                    TotalQuantity = int.Parse(NewQuantityForMenuProduct)
                });
            }
        }

        public ICommand AddProductToMenu
        {
            get
            {
                if (m_addProductToMenu == null)
                    m_addProductToMenu = new RelayCommand<object>(AddProdToMenu);
                return m_addProductToMenu;
            }
        }

        private void AddMenuPhoto(object parameter)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";

            if (op.ShowDialog() == true)
            {
                MenuImages.Add(op.FileName);
                NotifyPropertyChanged("MenuImages");
            }
        }

        public ICommand AddPhotoMenu
        {
            get
            {
                if (m_addPhotoToMenu == null)
                    m_addPhotoToMenu = new RelayCommand<object>(AddMenuPhoto);
                return m_addPhotoToMenu;
            }
        }

        public ClientProduct SelectedItemToRemove
        {
            get;
            set;
        }

        private void RemoveItemFromCommand(object parameter)
        {
            if (SelectedItemToRemove != null)
                for (int index = 0; index < ClientProductsList.Count; ++index)
                    if (ClientProductsList[index].Name.IndexOf(SelectedItemToRemove.Name) != -1)
                    {
                        if (ClientProductsList[index].NumberOfPieces - 1 == 0)
                        {
                            ClientProductsList.RemoveAt(index);
                            return;
                        }
                        else
                        {
                            ClientProductsList[index].NumberOfPieces -= 1;
                            NotifyPropertyChanged("ClientProductsList");
                        }
                    }
        }

        public ICommand DeleteItemFromCommand
        {
            get
            {
                if (m_deleteProductFromCommand == null)
                    m_deleteProductFromCommand = new RelayCommand<object>(RemoveItemFromCommand);
                return m_deleteProductFromCommand;
            }
        }

        public ObservableCollection<ComboBoxItem> ComboBoxItems
        {
            get
            {
                ObservableCollection<ComboBoxItem> items = new ObservableCollection<ComboBoxItem>();

                AllergensBll alergensBll = new AllergensBll();
                ObservableCollection<Allergens> alergens = alergensBll.GetAllAlergens();

                ComboBoxItem emptyItem = new ComboBoxItem();
                items.Add(emptyItem);

                for (int index = 0; index < alergens.Count; ++index)
                {
                    ComboBoxItem item = new ComboBoxItem();
                    item.Content = alergens[index].AllergenName;
                    items.Add(item);
                }

                return items;
            }
        }
    }
}
