﻿using BBRestaurant.Models.BusinessLogicLayer;
using BBRestaurant.Models.EntityLayer;
using System.Windows.Input;

namespace BBRestaurant.ViewModels
{
    class CommandHandlerCreateAccountPage : BasePropertyChanged
    {
        ClientBll m_clientBll;
        ICommand m_register;

        public CommandHandlerCreateAccountPage()
        {
            m_clientBll = new ClientBll();
        }

        public ICommand AddNewClient
        {
            get
            {
                if (m_register == null)
                    m_register = new RelayCommand<Client>(m_clientBll.AddClient);
                return m_register;
            }
        }
    }
}
